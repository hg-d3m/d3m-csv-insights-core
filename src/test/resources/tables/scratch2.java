import com.google.common.collect.Maps;

import java.util.Map;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.stream.Collectors;

public class Scratch2 {
    public static void main(String[] args) {

        Map<String, Long> map = Maps.newHashMap();
        map.put("a", 1);
        map.put("b", 2);
        map.put("c", 0);
        map.put("d", 4);

        ConcurrentSkipListMap<String, Long> frequenciesSorted = map.entrySet()
                .stream()
                .sorted((e1, e2) -> (int) (e1.getValue() - e2.getValue()))
                .limit(10)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, Long::sum, ConcurrentSkipListMap::new));

        System.out.println(frequenciesSorted);
    }


}