package com.hyperiongray.d3m.csvinsights.core.s3;

import com.hyperiongray.d3m.csvinsights.core.DefaultDatasetProcessor;
import com.hyperiongray.d3m.csvinsights.core.model.Dataset;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;


public class DefaultDatasetProcessorShouldWorkTest {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final DefaultDatasetProcessor defaultDatasetProcessor = new DefaultDatasetProcessor();

    @Test
    public void test1() {
        String path = "src/test/resources/tables/nulls/file1.csv";
        Dataset dataset = defaultDatasetProcessor.process(new File(path).toPath());
        Assert.assertEquals(37, dataset.getRows());
        Assert.assertEquals(3, dataset.getColumns().size());
    }

    @Test
    public void test2() {
        String path = "src/test/resources/tables/nulls/file2.csv";
        Dataset dataset = defaultDatasetProcessor.process(new File(path).toPath());
        Assert.assertEquals(431, dataset.getRows());
        Assert.assertEquals(6, dataset.getColumns().size());
    }

    @Test
    public void test3() {
        String path = "src/test/resources/tables/nulls/file3.csv";
        Dataset dataset = defaultDatasetProcessor.process(new File(path).toPath());
        Assert.assertEquals(37, dataset.getRows());
        Assert.assertEquals(3, dataset.getColumns().size());
    }

}
