package com.hyperiongray.d3m.csvinsights.core;

import com.hyperiongray.d3m.csvinsights.core.model.Dataset;
import com.hyperiongray.d3m.csvinsights.core.model.PrimitiveFieldType;
import com.hyperiongray.d3m.csvinsights.core.model.column.Column;
import com.hyperiongray.d3m.csvinsights.core.model.column.impl.StringColumn;
import com.hyperiongray.d3m.csvinsights.core.service.util.ClassUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;


public class DefaultDatasetProcessorTestSoccer {

    private final DefaultDatasetProcessor defaultDatasetProcessor = new DefaultDatasetProcessor();

    @Test
    public void processSoccer() {
        String path = "src/test/resources/tables/Men+Soccer+RW2019+10+28.csv";
        Dataset dataset = defaultDatasetProcessor.process(new File(path).toPath());
        checkColumn0(dataset.getColumns().get(0));
        checkColumn1(dataset.getColumns().get(1));
        checkColumn2(dataset.getColumns().get(2));
        checkColumn3(dataset.getColumns().get(3));
    }

    private void checkColumn0(Column column) {
        Assert.assertEquals("Order", column.getField().getName());
        Assert.assertEquals(0, column.getField().getColumnIndex());
        Assert.assertEquals(PrimitiveFieldType.LONG, column.getField().getParser().getPrimitiveFieldType());
        Assert.assertEquals(419, column.getSummary().getCount());
        Assert.assertEquals(419, column.getSummary().getDistinct());
        Assert.assertFalse(column.getSummary().isNullable());
        Assert.assertTrue(column.getSummary().isPrimaryKeyCandidate());
        Assert.assertTrue(column.getSummary().isUnique());
        Assert.assertEquals(419, column.getSummary().getNotNulls());
    }

    private void checkColumn1(Column column) {
        Assert.assertEquals("Team", column.getField().getName());
        Assert.assertEquals(1, column.getField().getColumnIndex());
        Assert.assertEquals(PrimitiveFieldType.STRING, column.getField().getParser().getPrimitiveFieldType());
        StringColumn stringColumn = ClassUtils.convertInstanceOfObject(column, StringColumn.class);
        Assert.assertEquals(419, column.getSummary().getCount());
        Assert.assertEquals(419, column.getSummary().getDistinct());
        Assert.assertFalse(column.getSummary().isNullable());
        Assert.assertTrue(column.getSummary().isPrimaryKeyCandidate());
        Assert.assertTrue(column.getSummary().isUnique());
        Assert.assertEquals(419, column.getSummary().getNotNulls());
    }

    private void checkColumn2(Column column) {
        Assert.assertEquals("Rating", column.getField().getName());
        Assert.assertEquals(2, column.getField().getColumnIndex());
        Assert.assertEquals(PrimitiveFieldType.DOUBLE, column.getField().getParser().getPrimitiveFieldType());
        Assert.assertEquals(419, column.getSummary().getCount());
        Assert.assertEquals(419, column.getSummary().getDistinct());
        Assert.assertFalse(column.getSummary().isNullable());
        Assert.assertTrue(column.getSummary().isPrimaryKeyCandidate());
        Assert.assertTrue(column.getSummary().isUnique());
        Assert.assertEquals(419, column.getSummary().getNotNulls());
    }

    private void checkColumn3(Column column) {
        Assert.assertEquals("Conference", column.getField().getName());
        Assert.assertEquals(3, column.getField().getColumnIndex());
        Assert.assertEquals(PrimitiveFieldType.STRING, column.getField().getParser().getPrimitiveFieldType());
        StringColumn stringColumn = ClassUtils.convertInstanceOfObject(column, StringColumn.class);
        Assert.assertEquals(419, column.getSummary().getCount());
        Assert.assertEquals(46, column.getSummary().getDistinct());
        Assert.assertFalse(column.getSummary().isNullable());
        Assert.assertFalse(column.getSummary().isPrimaryKeyCandidate());
        Assert.assertFalse(column.getSummary().isUnique());
        Assert.assertEquals(419, column.getSummary().getNotNulls());
    }

}