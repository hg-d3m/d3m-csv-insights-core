package com.hyperiongray.d3m.csvinsights.core.service.fx;

import com.google.common.collect.Lists;
import com.hyperiongray.d3m.csvinsights.core.model.Field;
import com.hyperiongray.d3m.csvinsights.core.model.PrimitiveFieldType;
import com.hyperiongray.d3m.csvinsights.core.service.FieldInferrerService;
import com.hyperiongray.d3m.csvinsights.core.model.Schema;
import com.hyperiongray.d3m.csvinsights.core.service.SchemaInferrerService;
import com.hyperiongray.d3m.csvinsights.core.source.RawDataset;
import com.hyperiongray.d3m.csvinsights.core.source.reader.DatasetReaderConfig;
import com.hyperiongray.d3m.csvinsights.core.source.reader.impl.LocalDatasetReader;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.util.List;

public class SchemaInferrerServiceTest {

    private final FieldInferrerService fieldInferrerService = new FieldInferrerService();
    private final SchemaInferrerService schemaInferrerService = new SchemaInferrerService(fieldInferrerService);

    @Test
    public void infer() {

        List<List<String>> rows = Lists.newArrayList();
        List<String> row1 = Lists.newArrayList("1", "Green", "2020-05-04", "0.01");
        rows.add(row1);
        List<String> row2 = Lists.newArrayList("2", "Red", "2020-05-05", "0.02");
        rows.add(row2);
        List<String> row3 = Lists.newArrayList("3", "Blue", "2020-05-06", "0.03");
        rows.add(row3);

        List<String> headers = Lists.newArrayList("Rank", "Color", "Date", "Price");


        RawDataset rawDataset = RawDataset.newBuilder().withHeaders(headers).withRows(rows).build();
//        List<Field> fields =
        Schema schema = schemaInferrerService.inferSchema(rawDataset, 2);

        int i = 0;
        Field field = schema.getFields().get(i);
        Assert.assertEquals(0, field.getColumnIndex());
        Assert.assertEquals("Rank", field.getName());
        Assert.assertEquals(PrimitiveFieldType.LONG, field.getParser().getPrimitiveFieldType());

        i++;
        field = schema.getFields().get(i);
        Assert.assertEquals(1, field.getColumnIndex());
        Assert.assertEquals("Color", field.getName());
        Assert.assertEquals(PrimitiveFieldType.STRING, field.getParser().getPrimitiveFieldType());

        i++;
        field = schema.getFields().get(i);
        Assert.assertEquals(2, field.getColumnIndex());
        Assert.assertEquals("Date", field.getName());
        Assert.assertEquals(PrimitiveFieldType.DATE, field.getParser().getPrimitiveFieldType());

        i++;
        field = schema.getFields().get(i);
        Assert.assertEquals(3, field.getColumnIndex());
        Assert.assertEquals("Price", field.getName());
        Assert.assertEquals(PrimitiveFieldType.DOUBLE, field.getParser().getPrimitiveFieldType());

    }

    @Test
    public void inferSchema() {
        String path = "src/test/resources/tables/GDP.csv";
        LocalDatasetReader localDatasetReader = new LocalDatasetReader(new File(path).toPath(), DatasetReaderConfig.getDefault());
        RawDataset rawDataset = localDatasetReader.readAsDatasetRaw();
        Schema schema = schemaInferrerService.inferSchema(rawDataset, 10);
        System.out.println(schema);

        Assert.assertTrue(schema.hasHeaders());

        int i = 0;
        Field field = schema.getFields().get(i);
        Assert.assertEquals(0, field.getColumnIndex());
        Assert.assertEquals("Country", field.getName());
        Assert.assertEquals(PrimitiveFieldType.STRING, field.getParser().getPrimitiveFieldType());

        i++;
        field = schema.getFields().get(i);
        Assert.assertEquals(1, field.getColumnIndex());
        Assert.assertEquals("Ranking", field.getName());
        Assert.assertEquals(PrimitiveFieldType.LONG, field.getParser().getPrimitiveFieldType());

        i++;
        field = schema.getFields().get(i);
        Assert.assertEquals(2, field.getColumnIndex());
        Assert.assertEquals("Country2", field.getName());
        Assert.assertEquals(PrimitiveFieldType.STRING, field.getParser().getPrimitiveFieldType());

        i++;
        field = schema.getFields().get(i);
        Assert.assertEquals(3, field.getColumnIndex());
        Assert.assertEquals("GDP", field.getName());
        Assert.assertEquals(PrimitiveFieldType.LONG, field.getParser().getPrimitiveFieldType());

    }
}