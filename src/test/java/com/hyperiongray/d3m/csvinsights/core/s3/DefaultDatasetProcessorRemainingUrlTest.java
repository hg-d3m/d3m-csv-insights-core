package com.hyperiongray.d3m.csvinsights.core.s3;

import com.google.common.collect.Lists;
import com.hyperiongray.d3m.csvinsights.core.DefaultDatasetProcessor;
import com.hyperiongray.d3m.csvinsights.core.model.Dataset;
import com.hyperiongray.d3m.csvinsights.core.service.exceptions.FailedToParseDatasetException;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.mapping;
import static java.util.stream.Collectors.toList;


public class DefaultDatasetProcessorRemainingUrlTest {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private List<String> allUrls = Lists.newArrayList(
    "https://d3m-test.s3-us-west-2.amazonaws.com/downloaded-files/crawl-tasks/DSCVRY-bf9b732e-1aff-4d6d-8eef-d0766a0c33fd/catalog.data.gov/02097ea7-6122-46df-a66d-d82dd59476b5/filename-e0fd63e3-7a7b-11ea-8560-99ef94b15629",
    "https://d3m-test.s3-us-west-2.amazonaws.com/downloaded-files/crawl-tasks/DSCVRY-5dd20e6d-b385-4fa3-a601-d60fe421b8b2/catalog.data.gov/9f2d7224-c62d-47e3-b5e6-ccf13336b8f3/filename-da2fce40-7a80-11ea-8082-eb53fc7f14d5",
    "https://d3m-test.s3-us-west-2.amazonaws.com/downloaded-files/crawl-tasks/DSCVRY-d3a5c287-271e-40d2-b4d9-747efc43c952/datacatalog.worldbank.org/6a6d23e0-6346-43ba-978e-2dc1f693238c/filename-99737887-41bc-11ea-b1c5-6dd673058d1e",
    "https://d3m-test.s3-us-west-2.amazonaws.com/downloaded-files/crawl-tasks/DSCVRY-6546f24c-5c06-412d-a6c1-4473a4754075/datacatalog.worldbank.org/facc504a-2b41-4911-a481-2babe9dcea0b/filename-2d6feb1a-46d1-11ea-9748-096626e5f062",
    "https://d3m-test.s3-us-west-2.amazonaws.com/downloaded-files/crawl-tasks/DSCVRY-bf9b732e-1aff-4d6d-8eef-d0766a0c33fd/data.gov.uk/5ada01cd-23eb-44b2-a732-d11b78fbc956/filename-e0fd63b9-7a7b-11ea-8560-99ef94b15629",
    "https://d3m-test.s3-us-west-2.amazonaws.com/downloaded-files/crawl-tasks/DSCVRY-5dd20e6d-b385-4fa3-a601-d60fe421b8b2/www.ons.gov.uk/e9bf5b53-d532-4af8-a2f4-4d252bc1da05/filename-da2ff511-7a80-11ea-8082-eb53fc7f14d5",
    "https://d3m-test.s3-us-west-2.amazonaws.com/downloaded-files/crawl-tasks/DSCVRY-5dd20e6d-b385-4fa3-a601-d60fe421b8b2/data.gov.uk/bc1ea115-0b5a-4f92-8bfc-d0715cefc9b1/filename-da2ff532-7a80-11ea-8082-eb53fc7f14d5",
    "https://d3m-test.s3-us-west-2.amazonaws.com/downloaded-files/crawl-tasks/DSCVRY-2f05f629-c807-42c9-80d9-1fdfa14ac637/data.gov.uk/b8c59ff5-743c-4127-aeeb-310795534e01/filename-704dc0e3-55ca-11ea-8321-71c7e060567f",
    "https://d3m-test.s3-us-west-2.amazonaws.com/downloaded-files/crawl-tasks/DSCVRY-5dd20e6d-b385-4fa3-a601-d60fe421b8b2/data.gov.uk/e3ce67cc-cd07-4b56-b4e0-6935a943bec1/filename-da2ff55b-7a80-11ea-8082-eb53fc7f14d5",
    "https://d3m-test.s3-us-west-2.amazonaws.com/downloaded-files/crawl-tasks/DSCVRY-2f05f629-c807-42c9-80d9-1fdfa14ac637/data.gov.uk/bed8c166-60e7-4929-9694-5b699cf36a3a/filename-704e0ee9-55ca-11ea-8321-71c7e060567f",
    "https://d3m-test.s3-us-west-2.amazonaws.com/downloaded-files/crawl-tasks/DSCVRY-5dd20e6d-b385-4fa3-a601-d60fe421b8b2/data.gov.uk/0269ece1-63cc-474c-9a25-bb5b6a89800d/filename-da2fce61-7a80-11ea-8082-eb53fc7f14d5",
    "https://d3m-test.s3-us-west-2.amazonaws.com/downloaded-files/crawl-tasks/DSCVRY-6546f24c-5c06-412d-a6c1-4473a4754075/figshare.com/65437cce-0381-48a7-a469-6eeb7d3be298/filename-2d701252-46d1-11ea-9748-096626e5f062",
    "https://d3m-test.s3-us-west-2.amazonaws.com/downloaded-files/crawl-tasks/DSCVRY-481a0de4-010b-4913-a8e8-e089d16f54a8/www.ons.gov.uk/014c2a74-e16b-49b7-8964-f8968a5ed387/filename-aed25c91-41fb-11ea-8397-99608af9be27",
    "https://d3m-test.s3-us-west-2.amazonaws.com/downloaded-files/crawl-tasks/DSCVRY-481a0de4-010b-4913-a8e8-e089d16f54a8/www.ons.gov.uk/dbbc4418-a59c-45c1-85a0-fefec9a7c734/filename-aed2aabe-41fb-11ea-8397-99608af9be27",
    "https://d3m-test.s3-us-west-2.amazonaws.com/downloaded-files/crawl-tasks/DSCVRY-5dd20e6d-b385-4fa3-a601-d60fe421b8b2/data.gov.uk/9779f224-e907-4ea2-859c-5822c62bc041/filename-da2ff53c-7a80-11ea-8082-eb53fc7f14d5",
    "https://d3m-test.s3-us-west-2.amazonaws.com/downloaded-files/crawl-tasks/DSCVRY-bf9b732e-1aff-4d6d-8eef-d0766a0c33fd/data.gov.uk/dfc7984e-c331-4122-ada2-d632aad6ce0e/filename-e0fd63d8-7a7b-11ea-8560-99ef94b15629",
    "https://d3m-test.s3-us-west-2.amazonaws.com/downloaded-files/crawl-tasks/DSCVRY-5dd20e6d-b385-4fa3-a601-d60fe421b8b2/data.gov.uk/98ae6708-d173-4079-b5fb-f117eb1f5980/filename-da2ff515-7a80-11ea-8082-eb53fc7f14d5",
    "https://d3m-test.s3-us-west-2.amazonaws.com/downloaded-files/crawl-tasks/DSCVRY-5dd20e6d-b385-4fa3-a601-d60fe421b8b2/www.ons.gov.uk/eb0e6edc-785e-424b-a550-47ec4cb1491b/filename-da2fce02-7a80-11ea-8082-eb53fc7f14d5",
    "https://d3m-test.s3-us-west-2.amazonaws.com/downloaded-files/crawl-tasks/DSCVRY-2f05f629-c807-42c9-80d9-1fdfa14ac637/www.ons.gov.uk/86c102e1-f60c-426e-93ac-41ba195d3ee7/filename-704e35f8-55ca-11ea-8321-71c7e060567f",
    "https://d3m-test.s3-us-west-2.amazonaws.com/downloaded-files/crawl-tasks/DSCVRY-2f05f629-c807-42c9-80d9-1fdfa14ac637/catalog.data.gov/8b641dfc-8a3a-4a79-acf2-f7b1f183df1f/filename-704de7e9-55ca-11ea-8321-71c7e060567f",
    "https://d3m-test.s3-us-west-2.amazonaws.com/downloaded-files/crawl-tasks/DSCVRY-5dd20e6d-b385-4fa3-a601-d60fe421b8b2/www.ons.gov.uk/94658c7e-12fb-4a8c-aef1-74f97e345a5b/filename-da2fce02-7a80-11ea-8082-eb53fc7f14d5",
    "https://d3m-test.s3-us-west-2.amazonaws.com/downloaded-files/crawl-tasks/DSCVRY-bf9b732e-1aff-4d6d-8eef-d0766a0c33fd/catalog.data.gov/91cbe09e-9263-45ee-8644-70e6f5c71f93/filename-e0fd63d3-7a7b-11ea-8560-99ef94b15629",
    "https://d3m-test.s3-us-west-2.amazonaws.com/downloaded-files/crawl-tasks/DSCVRY-bf9b732e-1aff-4d6d-8eef-d0766a0c33fd/www.ons.gov.uk/5bcbe45f-ccb0-41bb-af39-d05d1122b5dd/filename-e0fd3c81-7a7b-11ea-8560-99ef94b15629",
    "https://d3m-test.s3-us-west-2.amazonaws.com/downloaded-files/crawl-tasks/DSCVRY-6ad9393c-345f-45c3-b8d8-a23da19d2d42/data.gov.uk/4f506d06-64b2-4d08-86e9-e017a416c720/filename-fef4dc36-7b0e-11ea-9531-45618f527e45",
    "https://d3m-test.s3-us-west-2.amazonaws.com/downloaded-files/crawl-tasks/DSCVRY-2f05f629-c807-42c9-80d9-1fdfa14ac637/datacatalog.worldbank.org/f7792569-299c-458b-8353-df4468ab7397/filename-704de7dd-55ca-11ea-8321-71c7e060567f",
    "https://d3m-test.s3-us-west-2.amazonaws.com/downloaded-files/crawl-tasks/DSCVRY-9dd04b4f-58ba-4680-a596-ed859bbc4e7a/data.gov.uk/4936c4fd-294b-4330-b6cb-02f38c824886/filename-e3bcea76-7b45-11ea-9531-45618f527e45",
    "https://d3m-test.s3-us-west-2.amazonaws.com/downloaded-files/crawl-tasks/DSCVRY-2f05f629-c807-42c9-80d9-1fdfa14ac637/data.gov.uk/db770604-876f-4b5c-910a-73e36e1d4547/filename-704de7de-55ca-11ea-8321-71c7e060567f",
    "https://d3m-test.s3-us-west-2.amazonaws.com/downloaded-files/crawl-tasks/DSCVRY-d2d648e4-baaa-40df-827e-d1520444b95e/datacatalog.worldbank.org/20989dd9-c4f7-4a59-ad73-f349236e33a5/filename-440df389-41c1-11ea-87bd-39de9f8455cc",
    "https://d3m-test.s3-us-west-2.amazonaws.com/downloaded-files/crawl-tasks/DSCVRY-2f05f629-c807-42c9-80d9-1fdfa14ac637/data.gov.uk/b03eb79b-d372-4c25-8fac-818d6add1038/filename-704dc0e2-55ca-11ea-8321-71c7e060567f",
    "https://d3m-test.s3-us-west-2.amazonaws.com/downloaded-files/crawl-tasks/DSCVRY-204f2957-8eab-4abf-92ed-01797f8ca432/datacatalog.worldbank.org/e301700b-6180-4ff8-a149-656807f0aadc/filename-e96e106d-41b2-11ea-b1c5-6dd673058d1e",
    "https://d3m-test.s3-us-west-2.amazonaws.com/downloaded-files/crawl-tasks/DSCVRY-2f05f629-c807-42c9-80d9-1fdfa14ac637/www.ons.gov.uk/29a669c4-67ff-4df7-b845-fe6e63420ed6/filename-704dc0e8-55ca-11ea-8321-71c7e060567f",
    "https://d3m-test.s3-us-west-2.amazonaws.com/downloaded-files/crawl-tasks/DSCVRY-5dd20e6d-b385-4fa3-a601-d60fe421b8b2/data.gov.uk/1b914aaf-59b0-4eb6-ae93-11e02f6a7d13/filename-da2fce61-7a80-11ea-8082-eb53fc7f14d5",
    "https://d3m-test.s3-us-west-2.amazonaws.com/uploaded-files/9cfb64cb-7c93-414b-984e-ce5a3f0fb0b7%2F012-human_development_reports.numbers",
    "https://d3m-test.s3-us-west-2.amazonaws.com/downloaded-files/crawl-tasks/DSCVRY-2f05f629-c807-42c9-80d9-1fdfa14ac637/www.ons.gov.uk/74a7ba34-b0bb-4d06-a769-8d4d01a28d25/filename-704e3600-55ca-11ea-8321-71c7e060567f",
    "https://d3m-test.s3-us-west-2.amazonaws.com/downloaded-files/crawl-tasks/DSCVRY-9dd04b4f-58ba-4680-a596-ed859bbc4e7a/data.gov.uk/896fc307-c46d-4175-b5c1-aa0f0197104d/filename-e3bd112a-7b45-11ea-9531-45618f527e45",
    "https://d3m-test.s3-us-west-2.amazonaws.com/downloaded-files/crawl-tasks/DSCVRY-bf9b732e-1aff-4d6d-8eef-d0766a0c33fd/data.gov.uk/29873cca-68bf-43f6-877d-90ddb0f98f41/filename-e0fd6392-7a7b-11ea-8560-99ef94b15629",
    "https://d3m-test.s3-us-west-2.amazonaws.com/downloaded-files/crawl-tasks/DSCVRY-5dd20e6d-b385-4fa3-a601-d60fe421b8b2/data.gov.uk/9a16069f-07f3-4e84-8da4-b1052f76b397/filename-da2ff53c-7a80-11ea-8082-eb53fc7f14d5",
    "https://d3m-test.s3-us-west-2.amazonaws.com/downloaded-files/crawl-tasks/DSCVRY-2f05f629-c807-42c9-80d9-1fdfa14ac637/www.ons.gov.uk/ace1d621-e6bd-4ccb-9090-5eaaa75277fd/filename-704e0ef3-55ca-11ea-8321-71c7e060567f",
    "https://d3m-test.s3-us-west-2.amazonaws.com/downloaded-files/crawl-tasks/DSCVRY-6ad9393c-345f-45c3-b8d8-a23da19d2d42/data.gov.uk/ab84aca9-b20d-4489-a443-d1962e7d4f37/filename-fef4dc0b-7b0e-11ea-9531-45618f527e45",
    "https://d3m-test.s3-us-west-2.amazonaws.com/downloaded-files/crawl-tasks/DSCVRY-5dd20e6d-b385-4fa3-a601-d60fe421b8b2/data.gov.uk/8307938d-75dc-41d3-b6b3-3283bf68ada6/filename-da2ff515-7a80-11ea-8082-eb53fc7f14d5",
    "https://d3m-test.s3-us-west-2.amazonaws.com/downloaded-files/crawl-tasks/DSCVRY-481a0de4-010b-4913-a8e8-e089d16f54a8/www.ons.gov.uk/430d398f-6ccb-4ca5-970a-3acf33640fe0/filename-aed283e6-41fb-11ea-8397-99608af9be27",
    "https://d3m-test.s3-us-west-2.amazonaws.com/downloaded-files/crawl-tasks/DSCVRY-bf9b732e-1aff-4d6d-8eef-d0766a0c33fd/www.ons.gov.uk/2c2de5b6-3d4a-4e82-93c3-6f9c13b7ea4c/filename-e0fd3d06-7a7b-11ea-8560-99ef94b15629",
    "https://d3m-test.s3-us-west-2.amazonaws.com/downloaded-files/crawl-tasks/DSCVRY-bf9b732e-1aff-4d6d-8eef-d0766a0c33fd/data.gov.uk/9ef7f068-02ae-453b-acde-e5e21e693be3/filename-e0fd3ce0-7a7b-11ea-8560-99ef94b15629",
    "https://d3m-test.s3-us-west-2.amazonaws.com/downloaded-files/crawl-tasks/DSCVRY-2f05f629-c807-42c9-80d9-1fdfa14ac637/www.ons.gov.uk/640baa0c-071f-4a2d-99cb-1775013e9762/filename-704e0ee2-55ca-11ea-8321-71c7e060567f",
    "https://d3m-test.s3-us-west-2.amazonaws.com/downloaded-files/crawl-tasks/DSCVRY-5dd20e6d-b385-4fa3-a601-d60fe421b8b2/data.gov.uk/38e99568-b868-4953-9e30-57537adfe885/filename-da2ff532-7a80-11ea-8082-eb53fc7f14d5",
    "https://d3m-test.s3-us-west-2.amazonaws.com/downloaded-files/crawl-tasks/DSCVRY-5dd20e6d-b385-4fa3-a601-d60fe421b8b2/data.gov.uk/6fedb413-bcc0-4b3a-8109-6e398e5f44dd/filename-da2ff55b-7a80-11ea-8082-eb53fc7f14d5",
    "https://d3m-test.s3-us-west-2.amazonaws.com/downloaded-files/crawl-tasks/DSCVRY-2f05f629-c807-42c9-80d9-1fdfa14ac637/www.ons.gov.uk/4a0e8bae-2bc7-4eb7-9cb6-44d6d76e8c40/filename-704e35f9-55ca-11ea-8321-71c7e060567f",
    "https://d3m-test.s3-us-west-2.amazonaws.com/downloaded-files/crawl-tasks/DSCVRY-2f05f629-c807-42c9-80d9-1fdfa14ac637/data.gov.uk/f2bac737-fde7-4f81-8f36-a37b21beba18/filename-704de7fe-55ca-11ea-8321-71c7e060567f",
    "https://d3m-test.s3-us-west-2.amazonaws.com/downloaded-files/crawl-tasks/DSCVRY-2f05f629-c807-42c9-80d9-1fdfa14ac637/www.ons.gov.uk/3184b76f-7309-4fe2-86a2-26aeae267d80/filename-704e0ef7-55ca-11ea-8321-71c7e060567f",
    "https://d3m-test.s3-us-west-2.amazonaws.com/downloaded-files/crawl-tasks/DSCVRY-6ad9393c-345f-45c3-b8d8-a23da19d2d42/www.ons.gov.uk/71482faf-80bd-4d22-9f84-cc67cc79ffb8/filename-fef418d5-7b0e-11ea-9531-45618f527e45",
    "https://d3m-test.s3-us-west-2.amazonaws.com/downloaded-files/crawl-tasks/DSCVRY-2f05f629-c807-42c9-80d9-1fdfa14ac637/www.ons.gov.uk/adda24b4-ce54-4978-b15f-9f0266327621/filename-704e360c-55ca-11ea-8321-71c7e060567f",
    "https://d3m-test.s3-us-west-2.amazonaws.com/downloaded-files/crawl-tasks/DSCVRY-bf9b732e-1aff-4d6d-8eef-d0766a0c33fd/catalog.data.gov/4a551321-1dfb-47e9-89b0-03bfd7e06fe3/filename-e0fd3cbf-7a7b-11ea-8560-99ef94b15629",
    "https://d3m-test.s3-us-west-2.amazonaws.com/downloaded-files/crawl-tasks/DSCVRY-d2d648e4-baaa-40df-827e-d1520444b95e/datacatalog.worldbank.org/7fafd3ab-86cd-4616-8846-5f49727dc9bf/filename-440df3bf-41c1-11ea-87bd-39de9f8455cc",
    "https://d3m-test.s3-us-west-2.amazonaws.com/downloaded-files/crawl-tasks/DSCVRY-6ad9393c-345f-45c3-b8d8-a23da19d2d42/data.gov.uk/a3f723d6-c516-43e1-bf66-7b9219004324/filename-fef48de8-7b0e-11ea-9531-45618f527e45",
    "https://d3m-test.s3-us-west-2.amazonaws.com/downloaded-files/crawl-tasks/DSCVRY-bf9b732e-1aff-4d6d-8eef-d0766a0c33fd/data.gov.uk/5127c308-1c7c-40a9-aebe-92687fa2fa31/filename-e0fd63af-7a7b-11ea-8560-99ef94b15629"
    );


    private final DefaultDatasetProcessor defaultDatasetProcessor = new DefaultDatasetProcessor();

    @Test
    @Ignore
    public void testFailedUrls() {
        DefaultDatasetProcessorRemainingUrlTest defaultDatasetProcessorAllUrlTest = new DefaultDatasetProcessorRemainingUrlTest();
        logger.info("size: " + defaultDatasetProcessorAllUrlTest.allUrls.size());
        Set<String> uniqueUrl = new HashSet<>(defaultDatasetProcessorAllUrlTest.allUrls);
        logger.info("uniqueUrl: " + uniqueUrl.size());

        List<String> failed = Lists.newArrayList();
        List<Pair<RuntimeException, String>> errorsTracking = Lists.newArrayList();
        List<Pair<String, Dataset>> success = Lists.newArrayList();

        uniqueUrl.forEach(url -> {
                    try {
                        logger.info(url);
                        Dataset dataset = defaultDatasetProcessor.process(new URL(url));
                        logger.info(dataset.toString());
                        success.add(new ImmutablePair<>(url, dataset));
                    } catch (IllegalArgumentException | FailedToParseDatasetException iae) {
                        logger.error(iae.getMessage(), iae);
                        errorsTracking.add(new ImmutablePair<>(iae, url));
                    } catch (Exception ex) {
                        logger.error("failed", ex);
                        failed.add(url);
                    }
                }
        );

        logger.info("success: " + success.size());
        logger.info("errorsTracking: " + errorsTracking.size());
        logger.info("failed: " + failed.size());


        logger.info(failed.toString());
        logger.info(errorsTracking.toString());

        Map<String, Long> collectedErrors = errorsTracking.stream().collect(Collectors.groupingBy(x -> x.getKey().getMessage(), Collectors.counting()));
        logger.info(collectedErrors.toString());

        Map<String, List<String>> collectedErrors2 = errorsTracking.stream().collect(Collectors.groupingBy(x -> x.getKey().getMessage(), mapping(x->x.getValue(), toList())));
        logger.info(collectedErrors2.toString());

    }
}
