package com.hyperiongray.d3m.csvinsights.core.parser.header;

import com.hyperiongray.d3m.csvinsights.core.source.parser.FieldParserUtil;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class FieldParserUtilTest {

    @Test
    public void parseLine() {
        String line = "abcd1|efgh2|ijkl3|mnop4";
        List<String> strings = FieldParserUtil.parseLine(line, '|');
        System.out.println(strings);
        Assert.assertEquals(4, strings.size());
        Assert.assertEquals("abcd1", strings.get(0));
        Assert.assertEquals("efgh2", strings.get(1));
        Assert.assertEquals("ijkl3", strings.get(2));
        Assert.assertEquals("mnop4", strings.get(3));
    }

    @Test
    public void parseLineQuoted() {
        String line = "\"Parent Department\",\"Organisation\",\"Unit\",\"Reporting Senior Post\",\"Grade\",\"Payscale Minimum (£)\",\"Payscale Maximum (£)\",\"Generic Job Title\",\"Number of Posts in FTE\",\"Professional/Occupational Group\"";
        List<String> strings = FieldParserUtil.parseLine(line, ',');
        System.out.println(strings);

        Assert.assertEquals(10, strings.size());
        Assert.assertEquals("Parent Department", strings.get(0));
        Assert.assertEquals("Organisation", strings.get(1));
        Assert.assertEquals("Unit", strings.get(2));
        Assert.assertEquals("Reporting Senior Post", strings.get(3));
        Assert.assertEquals("Grade", strings.get(4));
        Assert.assertEquals("Payscale Minimum (£)", strings.get(5));
        Assert.assertEquals("Payscale Maximum (£)", strings.get(6));
        Assert.assertEquals("Generic Job Title", strings.get(7));
        Assert.assertEquals("Number of Posts in FTE", strings.get(8));
        Assert.assertEquals("Professional/Occupational Group", strings.get(9));
    }

    @Test
    public void parseLineQuotedCommas() {
        String line = "\"Parent, Depart, ment\",\"Organisation\",\"Unit\",\"Reporting Senior Post\",\"Grade\",\"Payscale Minimum (£)\",\"Payscale Maximum (£)\",\"Generic Job Title\",\"Number of Posts in FTE\",\"Professional/Occupational Group\"";
        List<String> strings = FieldParserUtil.parseLine(line, ',');
        System.out.println(strings);

        Assert.assertEquals(10, strings.size());
        Assert.assertEquals("Parent, Depart, ment", strings.get(0));
        Assert.assertEquals("Organisation", strings.get(1));
        Assert.assertEquals("Unit", strings.get(2));
        Assert.assertEquals("Reporting Senior Post", strings.get(3));
        Assert.assertEquals("Grade", strings.get(4));
        Assert.assertEquals("Payscale Minimum (£)", strings.get(5));
        Assert.assertEquals("Payscale Maximum (£)", strings.get(6));
        Assert.assertEquals("Generic Job Title", strings.get(7));
        Assert.assertEquals("Number of Posts in FTE", strings.get(8));
        Assert.assertEquals("Professional/Occupational Group", strings.get(9));
    }


    @Test
    public void parseLineQuotedStrings1() {

        String line = "Column 1,\"Column \"\"With Quoted\"\" Title\",Third Column";
        List<String> strings = FieldParserUtil.parseLine(line, ',');
        System.out.println(strings);
        Assert.assertEquals(3, strings.size());
        Assert.assertEquals("Column 1", strings.get(0));
        Assert.assertEquals("Column \"With Quoted\" Title", strings.get(1));
        Assert.assertEquals("Third Column", strings.get(2));
    }

    @Test
    public void parseLineQuotedStrings2() {

        String line = "Lorem Ipsum,Something Else,\"Value \"\"With Quoted\"\" content\"";
        List<String> strings = FieldParserUtil.parseLine(line, ',');
        System.out.println(strings);
        Assert.assertEquals(3, strings.size());
        Assert.assertEquals("Lorem Ipsum", strings.get(0));
        Assert.assertEquals("Something Else", strings.get(1));
        Assert.assertEquals("Value \"With Quoted\" content", strings.get(2));
    }

    @Test
    public void parseLineQuotedStrings3() {

        String line = "Another Value,\"Yet another \"\"Quoted String\"\" value\",Lorem Ipsum";
        List<String> strings = FieldParserUtil.parseLine(line, ',');
        System.out.println(strings);
        Assert.assertEquals(3, strings.size());
        Assert.assertEquals("Another Value", strings.get(0));
        Assert.assertEquals("Yet another \"Quoted String\" value", strings.get(1));
        Assert.assertEquals("Lorem Ipsum", strings.get(2));
    }


    @Test
    public void parseLineQuotedStrings4() {

        String line = "\"Yet another \"\"Quoted String\"\" value\",Lorem Ipsum,Something Else";
        List<String> strings = FieldParserUtil.parseLine(line, ',');
        System.out.println(strings);
        Assert.assertEquals(3, strings.size());
        Assert.assertEquals("Yet another \"Quoted String\" value", strings.get(0));
        Assert.assertEquals("Lorem Ipsum", strings.get(1));
        Assert.assertEquals("Something Else", strings.get(2));
    }



    @Test
    public void parseLineQuotedStrings4Commas() {

        String line = "\"Yet another \"\"Quoted ,String\"\" value\",Lorem Ipsum,Something Else";
        List<String> strings = FieldParserUtil.parseLine(line, ',');
        System.out.println(strings);
        Assert.assertEquals(3, strings.size());
        Assert.assertEquals("Yet another \"Quoted ,String\" value", strings.get(0));
        Assert.assertEquals("Lorem Ipsum", strings.get(1));
        Assert.assertEquals("Something Else", strings.get(2));
    }

    @Test
    public void parseLineQuotedStrings4When2Commas() {

        String line = "\"Yet ,another \"\"Quoted ,String\"\" value\",Lorem Ipsum,Something Else";
        List<String> strings = FieldParserUtil.parseLine(line, ',');
        System.out.println(strings);
        Assert.assertEquals(3, strings.size());
        Assert.assertEquals("Yet ,another \"Quoted ,String\" value", strings.get(0));
        Assert.assertEquals("Lorem Ipsum", strings.get(1));
        Assert.assertEquals("Something Else", strings.get(2));
    }


    @Test
    public void parseLineQuotedStringsWhenDelimiterInQuotedText() {

        String line = "Yet another \"Quoted ,String\" value,Lorem Ipsum,Something Else";
        List<String> strings = FieldParserUtil.parseLine(line, ',');
        System.out.println(strings);
        Assert.assertEquals(3, strings.size());
        Assert.assertEquals("Yet another \"Quoted ,String\" value", strings.get(0));
        Assert.assertEquals("Lorem Ipsum", strings.get(1));
        Assert.assertEquals("Something Else", strings.get(2));
    }

}