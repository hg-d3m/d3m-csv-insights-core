package com.hyperiongray.d3m.csvinsights.core;

import com.hyperiongray.d3m.csvinsights.core.model.Dataset;
import com.hyperiongray.d3m.csvinsights.core.model.PrimitiveFieldType;
import com.hyperiongray.d3m.csvinsights.core.model.column.Column;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;

public class DefaultDatasetProcessorTestTraffic {

    private final DefaultDatasetProcessor defaultDatasetProcessor = new DefaultDatasetProcessor();

    @Test
    public void process2() {
        String path = "src/test/resources/tables/RTC 2018_Leeds.csv";
        Dataset dataset = defaultDatasetProcessor.process(new File(path).toPath());
        Assert.assertTrue(dataset.hasHeaders());
        Assert.assertEquals(1995, dataset.getRows());


        checkColumn0(dataset.getColumns().get(0));
        checkColumn1(dataset.getColumns().get(1));
        checkColumn2(dataset.getColumns().get(2));
        checkColumn3(dataset.getColumns().get(3));
        checkColumn4(dataset.getColumns().get(4));
        checkColumn5(dataset.getColumns().get(5));
        checkColumn6(dataset.getColumns().get(6));
        checkColumn7(dataset.getColumns().get(7));
        checkColumn8(dataset.getColumns().get(8));
        checkColumn9(dataset.getColumns().get(9));
        checkColumn10(dataset.getColumns().get(10));
        checkColumn11(dataset.getColumns().get(11));
        checkColumn12(dataset.getColumns().get(12));
        checkColumn13(dataset.getColumns().get(13));
        checkColumn14(dataset.getColumns().get(14));
        checkColumn15(dataset.getColumns().get(15));
        checkColumn16(dataset.getColumns().get(16));
        checkColumn17(dataset.getColumns().get(17));
        checkColumn18(dataset.getColumns().get(18));
        checkColumn19(dataset.getColumns().get(19));
        checkColumn20(dataset.getColumns().get(20));
        System.out.println(dataset);

    }

    private void checkColumn0(Column column) {
        Assert.assertEquals("Accident Fields_Reference Number", column.getField().getName());
        Assert.assertEquals(0, column.getField().getColumnIndex());
        Assert.assertEquals(PrimitiveFieldType.STRING, column.getField().getParser().getPrimitiveFieldType());
    }

    private void checkColumn1(Column column) {
        Assert.assertEquals("Grid Ref: Easting", column.getField().getName());
        Assert.assertEquals(1, column.getField().getColumnIndex());
        Assert.assertEquals(PrimitiveFieldType.LONG, column.getField().getParser().getPrimitiveFieldType());
    }

    private void checkColumn2(Column column) {
        Assert.assertEquals("Grid Ref: Northing", column.getField().getName());
        Assert.assertEquals(2, column.getField().getColumnIndex());
        Assert.assertEquals(PrimitiveFieldType.LONG, column.getField().getParser().getPrimitiveFieldType());
    }

    private void checkColumn3(Column column) {
        Assert.assertEquals("Number of Vehicles", column.getField().getName());
        Assert.assertEquals(3, column.getField().getColumnIndex());
        Assert.assertEquals(PrimitiveFieldType.LONG, column.getField().getParser().getPrimitiveFieldType());
    }

    private void checkColumn4(Column column) {
        Assert.assertEquals("Accident Date", column.getField().getName());
        Assert.assertEquals(4, column.getField().getColumnIndex());
        Assert.assertEquals(PrimitiveFieldType.DATE, column.getField().getParser().getPrimitiveFieldType());
    }

    private void checkColumn5(Column column) {
        Assert.assertEquals("Time (24hr)", column.getField().getName());
        Assert.assertEquals(5, column.getField().getColumnIndex());
        Assert.assertEquals(PrimitiveFieldType.LONG, column.getField().getParser().getPrimitiveFieldType());
    }

    private void checkColumn6(Column column) {
        Assert.assertEquals("1st Road Class", column.getField().getName());
        Assert.assertEquals(6, column.getField().getColumnIndex());
        Assert.assertEquals(PrimitiveFieldType.LONG, column.getField().getParser().getPrimitiveFieldType());
    }

    private void checkColumn7(Column column) {
        Assert.assertEquals("1st Road Class & No", column.getField().getName());
        Assert.assertEquals(7, column.getField().getColumnIndex());
        Assert.assertEquals(PrimitiveFieldType.STRING, column.getField().getParser().getPrimitiveFieldType());
    }

    private void checkColumn8(Column column) {
        Assert.assertEquals("Road Surface", column.getField().getName());
        Assert.assertEquals(8, column.getField().getColumnIndex());
        Assert.assertEquals(PrimitiveFieldType.LONG, column.getField().getParser().getPrimitiveFieldType());
    }

    private void checkColumn9(Column column) {
        Assert.assertEquals("Lighting Conditions", column.getField().getName());
        Assert.assertEquals(9, column.getField().getColumnIndex());
        Assert.assertEquals(PrimitiveFieldType.LONG, column.getField().getParser().getPrimitiveFieldType());
    }

    private void checkColumn10(Column column) {
        Assert.assertEquals("Weather Conditions", column.getField().getName());
        Assert.assertEquals(10, column.getField().getColumnIndex());
        Assert.assertEquals(PrimitiveFieldType.LONG, column.getField().getParser().getPrimitiveFieldType());
    }

    private void checkColumn11(Column column) {
        Assert.assertEquals("Local Authority", column.getField().getName());
        Assert.assertEquals(11, column.getField().getColumnIndex());
        Assert.assertEquals(PrimitiveFieldType.STRING, column.getField().getParser().getPrimitiveFieldType());
    }

    private void checkColumn12(Column column) {
        Assert.assertEquals("Vehicle Fields_Reference Number", column.getField().getName());
        Assert.assertEquals(12, column.getField().getColumnIndex());
        Assert.assertEquals(PrimitiveFieldType.STRING, column.getField().getParser().getPrimitiveFieldType());

    }


    private void checkColumn13(Column column) {
        Assert.assertEquals("Vehicle Number", column.getField().getName());
        Assert.assertEquals(13, column.getField().getColumnIndex());
        Assert.assertEquals(PrimitiveFieldType.LONG, column.getField().getParser().getPrimitiveFieldType());

    }

    private void checkColumn14(Column column) {
        Assert.assertEquals("Type of Vehicle", column.getField().getName());
        Assert.assertEquals(14, column.getField().getColumnIndex());
        Assert.assertEquals(PrimitiveFieldType.LONG, column.getField().getParser().getPrimitiveFieldType());

    }

    private void checkColumn15(Column column) {
        Assert.assertEquals("Casualty Fields_Reference Number", column.getField().getName());
        Assert.assertEquals(15, column.getField().getColumnIndex());
        Assert.assertEquals(PrimitiveFieldType.STRING, column.getField().getParser().getPrimitiveFieldType());

    }

    private void checkColumn16(Column column) {
        Assert.assertEquals("Casualty Veh No", column.getField().getName());
        Assert.assertEquals(16, column.getField().getColumnIndex());
        Assert.assertEquals(PrimitiveFieldType.LONG, column.getField().getParser().getPrimitiveFieldType());

    }

    private void checkColumn17(Column column) {
        Assert.assertEquals("Casualty Class", column.getField().getName());
        Assert.assertEquals(17, column.getField().getColumnIndex());
        Assert.assertEquals(PrimitiveFieldType.LONG, column.getField().getParser().getPrimitiveFieldType());

    }

    private void checkColumn18(Column column) {
        Assert.assertEquals("Casualty Severity", column.getField().getName());
        Assert.assertEquals(18, column.getField().getColumnIndex());
        Assert.assertEquals(PrimitiveFieldType.LONG, column.getField().getParser().getPrimitiveFieldType());

    }

    private void checkColumn19(Column column) {
        Assert.assertEquals("Sex of Casualty", column.getField().getName());
        Assert.assertEquals(19, column.getField().getColumnIndex());
        Assert.assertEquals(PrimitiveFieldType.LONG, column.getField().getParser().getPrimitiveFieldType());

    }

    private void checkColumn20(Column column) {
        Assert.assertEquals("Age of Casualty", column.getField().getName());
        Assert.assertEquals(20, column.getField().getColumnIndex());
        Assert.assertEquals(PrimitiveFieldType.LONG, column.getField().getParser().getPrimitiveFieldType());

    }

}