package com.hyperiongray.d3m.csvinsights.core;

import com.hyperiongray.d3m.csvinsights.core.model.Dataset;
import com.hyperiongray.d3m.csvinsights.core.model.PrimitiveFieldType;
import com.hyperiongray.d3m.csvinsights.core.model.column.Column;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;


public class DefaultDatasetProcessorTestPopData {

    private final DefaultDatasetProcessor defaultDatasetProcessor = new DefaultDatasetProcessor();


    @Test
    public void process() {
        String path = "src/test/resources/tables/pop-data.csv";
        Dataset dataset = defaultDatasetProcessor.process(new File(path).toPath());
        System.out.println(dataset);
        checkColumn0(dataset.getColumns().get(0));
        checkColumn1(dataset.getColumns().get(1));
        checkColumn2(dataset.getColumns().get(2));
        checkColumn3(dataset.getColumns().get(3));
        checkColumn4(dataset.getColumns().get(4));
        checkColumn5(dataset.getColumns().get(5));
        checkColumn6(dataset.getColumns().get(6));
        checkColumn7(dataset.getColumns().get(7));
        checkColumn8(dataset.getColumns().get(8));
        checkColumn9(dataset.getColumns().get(9));

    }

    private void checkColumn0(Column column) {
        Assert.assertEquals("dateRep", column.getField().getName());
        Assert.assertEquals(0, column.getField().getColumnIndex());
        Assert.assertEquals(PrimitiveFieldType.DATE, column.getField().getParser().getPrimitiveFieldType());
        Assert.assertEquals(PrimitiveFieldType.DATE, column.getField().getParser().getPrimitiveFieldType());
    }

    private void checkColumn1(Column column) {
        Assert.assertEquals("day", column.getField().getName());
        Assert.assertEquals(1, column.getField().getColumnIndex());
        Assert.assertEquals(PrimitiveFieldType.LONG, column.getField().getParser().getPrimitiveFieldType());
    }

    private void checkColumn2(Column column) {
        Assert.assertEquals("month", column.getField().getName());
        Assert.assertEquals(2, column.getField().getColumnIndex());
        Assert.assertEquals(PrimitiveFieldType.LONG, column.getField().getParser().getPrimitiveFieldType());
    }

    private void checkColumn3(Column column) {
        Assert.assertEquals("year", column.getField().getName());
        Assert.assertEquals(3, column.getField().getColumnIndex());
        Assert.assertEquals(PrimitiveFieldType.LONG, column.getField().getParser().getPrimitiveFieldType());
    }

    private void checkColumn4(Column column) {
        Assert.assertEquals("cases", column.getField().getName());
        Assert.assertEquals(4, column.getField().getColumnIndex());
        Assert.assertEquals(PrimitiveFieldType.LONG, column.getField().getParser().getPrimitiveFieldType());
    }

//    deaths,countriesAndTerritories,geoId,countryterritoryCode,popData2018

    private void checkColumn5(Column column) {
        Assert.assertEquals("deaths", column.getField().getName());
        Assert.assertEquals(5, column.getField().getColumnIndex());
        Assert.assertEquals(PrimitiveFieldType.LONG, column.getField().getParser().getPrimitiveFieldType());
    }

    private void checkColumn6(Column column) {
        Assert.assertEquals("countriesAndTerritories", column.getField().getName());
        Assert.assertEquals(6, column.getField().getColumnIndex());
        Assert.assertEquals(PrimitiveFieldType.STRING, column.getField().getParser().getPrimitiveFieldType());
    }

    private void checkColumn7(Column column) {
        Assert.assertEquals("geoId", column.getField().getName());
        Assert.assertEquals(7, column.getField().getColumnIndex());
        Assert.assertEquals(PrimitiveFieldType.STRING, column.getField().getParser().getPrimitiveFieldType());
    }


    private void checkColumn8(Column column) {
        Assert.assertEquals("countryterritoryCode", column.getField().getName());
        Assert.assertEquals(8, column.getField().getColumnIndex());
        Assert.assertEquals(PrimitiveFieldType.STRING, column.getField().getParser().getPrimitiveFieldType());
    }


    private void checkColumn9(Column column) {
        Assert.assertEquals("popData2018", column.getField().getName());
        Assert.assertEquals(9, column.getField().getColumnIndex());
        Assert.assertEquals(PrimitiveFieldType.LONG, column.getField().getParser().getPrimitiveFieldType());
    }

}