package com.hyperiongray.d3m.csvinsights.core.parser.header;

import com.hyperiongray.d3m.csvinsights.core.source.parser.FieldDelimiterUtil;
import org.junit.Assert;
import org.junit.Test;

import java.util.Optional;

public class FieldDelimiterUtilTest {


    @Test
    public void getSeparatorFromLineComma() {
        String line = "aafdf,aafd,adafda,afaf";
        Optional<Character> separatorFromLine = FieldDelimiterUtil.inferDelimiterFromLine(line);
        Assert.assertEquals(separatorFromLine.get(), Character.valueOf(','));
    }

    @Test
    public void getSeparatorFromLinePipe() {
        String line = "aafdf|aafd|adafda|afaf";
        Optional<Character> separatorFromLine = FieldDelimiterUtil.inferDelimiterFromLine(line);
        Assert.assertEquals(separatorFromLine.get(), Character.valueOf('|'));
    }

    @Test
    public void getSeparatorFromLineTab() {
        String line = "aafdf\taafd\tadafda\tafaf";
        Optional<Character> separatorFromLine = FieldDelimiterUtil.inferDelimiterFromLine(line);
        Assert.assertEquals(separatorFromLine.get(), Character.valueOf('\t'));
    }

    @Test
    public void getSeparatorFromLineAll() {
        String line = "aafdf\taafd\tadafda\tafaf,afaf,afaf";
        Optional<Character> separatorFromLine = FieldDelimiterUtil.inferDelimiterFromLine(line);
        Assert.assertEquals(separatorFromLine.get(), Character.valueOf('\t'));
    }

    @Test
    public void getSeparatorFromLineQuoted() {
        String line = "\"Parent Department\",\"Organisation\",\"Unit\",\"Reporting Senior Post\",\"Grade\",\"Payscale Minimum (£)\",\"Payscale Maximum (£)\",\"Generic Job Title\",\"Number of Posts in FTE\",\"Professional/Occupational Group\"";
        Optional<Character> separatorFromLine = FieldDelimiterUtil.inferDelimiterFromLine(line);
        Assert.assertEquals(separatorFromLine.get(), Character.valueOf(','));
    }

    @Test
    public void getSeparatorFromLineQuotedCommaInside() {
        String line = "\"Parent|||||||||||||||||Department\",\"Organisation\",\"Unit\",\"Reporting Senior Post\",\"Grade\",\"Payscale Minimum (£)\",\"Payscale Maximum (£)\",\"Generic Job Title\",\"Number of Posts in FTE\",\"Professional/Occupational Group\"";
        Optional<Character> separatorFromLine = FieldDelimiterUtil.inferDelimiterFromLine(line);
        Assert.assertEquals(separatorFromLine.get(), Character.valueOf(','));
    }


}