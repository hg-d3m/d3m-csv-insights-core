package com.hyperiongray.d3m.csvinsights.core.service.service;

import com.google.common.collect.Lists;
import com.hyperiongray.d3m.csvinsights.core.model.PrimitiveFieldType;
import com.hyperiongray.d3m.csvinsights.core.service.FieldInferrerService;
import com.hyperiongray.d3m.csvinsights.core.service.parser.Parser;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class FieldInferrerServiceTest {

    @Test
    public void infer() {
        List<String> inputs = Lists.newArrayList("yes", "no", "yes");
        Parser<?> parsers = new FieldInferrerService().infer(inputs);
        Assert.assertEquals(PrimitiveFieldType.BOOLEAN, parsers.getPrimitiveFieldType());

        System.out.println(parsers);
    }

    @Test
    public void inferLong() {
        List<String> inputs = Lists.newArrayList("11", "22", "333");
        Parser<?> parsers = new FieldInferrerService().infer(inputs);
        Assert.assertEquals(PrimitiveFieldType.LONG, parsers.getPrimitiveFieldType());
        System.out.println(parsers);
    }


    @Test
    public void inferDouble() {

        List<String> inputs = Lists.newArrayList("11.88", "22.55", "333.55");
        Parser<?> parsers = new FieldInferrerService().infer(inputs);
        Assert.assertEquals(PrimitiveFieldType.DOUBLE, parsers.getPrimitiveFieldType());
        System.out.println(parsers);
    }

    @Test
    public void inferTime() {

        List<String> inputs = Lists.newArrayList("11:28", "22:55", "03:55");
        Parser<?> parsers = new FieldInferrerService().infer(inputs);
        Assert.assertEquals(PrimitiveFieldType.TIME, parsers.getPrimitiveFieldType());
        System.out.println(parsers);
    }

    @Test
    public void inferDate() {
        List<String> inputs = Lists.newArrayList("11/28/2018", "12/12/2018", "10/10/2018");
        Parser<?> parsers = new FieldInferrerService().infer(inputs);
        Assert.assertEquals(PrimitiveFieldType.DATE, parsers.getPrimitiveFieldType());
        System.out.println(parsers);
    }

}