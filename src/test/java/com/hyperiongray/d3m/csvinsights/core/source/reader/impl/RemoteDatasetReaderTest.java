package com.hyperiongray.d3m.csvinsights.core.source.reader.impl;

import com.hyperiongray.d3m.csvinsights.core.source.reader.DatasetReaderConfig;
import org.junit.Test;

import java.net.MalformedURLException;
import java.net.URL;

public class RemoteDatasetReaderTest {

    @Test(expected = IllegalArgumentException.class)

    public void getFileSizeTest() {

        try {
            URL url = new URL("https://d3m-test.s3-us-west-2.amazonaws.com/downloaded-files/crawl-tasks/DSCVRY-481a0de4-010b-4913-a8e8-e089d16f54a8/catalog.data.gov/cfbfbbcc-6e4f-4f7e-bff0-0108c4e3d174/filename-aed25c9a-41fb-11ea-8397-99608af9be27");
            RemoteDatasetReader remoteDatasetReader = new RemoteDatasetReader(url, DatasetReaderConfig.getDefault());
            remoteDatasetReader.validateContent(url);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

    }


}