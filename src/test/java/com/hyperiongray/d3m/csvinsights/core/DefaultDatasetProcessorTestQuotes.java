package com.hyperiongray.d3m.csvinsights.core;

import com.hyperiongray.d3m.csvinsights.core.model.Dataset;
import com.hyperiongray.d3m.csvinsights.core.model.column.impl.StringColumn;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;

public class DefaultDatasetProcessorTestQuotes {

    private final DefaultDatasetProcessor defaultDatasetProcessor = new DefaultDatasetProcessor();

    @Test
    public void testCorrectProcessingOfValuesWithQuotes() {
        String path = "src/test/resources/tables/data with quotes.csv";
        Dataset dataset = defaultDatasetProcessor.process(new File(path).toPath());
        Assert.assertEquals("Yet another \"Quoted String\" value", ((StringColumn) dataset.getColumns().get(0)).getData().get(3));
        Assert.assertEquals("Yet another \"Quoted String\" value", ((StringColumn) dataset.getColumns().get(1)).getData().get(2));
        Assert.assertEquals("Column \"With Quoted\" Title", ((StringColumn) dataset.getColumns().get(1)).getData().get(0));
        Assert.assertEquals("Column \"With Quoted\" Title", ((StringColumn) dataset.getColumns().get(1)).getField().getName());
        Assert.assertEquals("Value \"With Quoted\" content", ((StringColumn) dataset.getColumns().get(2)).getData().get(1));
    }
}