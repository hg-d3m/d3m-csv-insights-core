package com.hyperiongray.d3m.csvinsights.core.source;

import com.hyperiongray.d3m.csvinsights.core.source.reader.DatasetReaderConfig;
import org.junit.Test;

import java.net.MalformedURLException;
import java.net.URL;

import static org.junit.Assert.fail;

public class RemoteTableTest {

    @Test
    public void ingest() {
        String dataSourceUrl = "https://d3m-test.s3-us-west-2.amazonaws.com/downloaded-files/crawl-tasks/DSCVRY-6ad9393c-345f-45c3-b8d8-a23da19d2d42/data.gov.uk/e56329fc-1967-43b7-8a94-10aa97fec8d7/filename-fef6ff1b-7b0e-11ea-9531-45618f527e45";
        try {
            RawDatasets.from(new URL(dataSourceUrl), DatasetReaderConfig.getDefault());
        } catch (MalformedURLException e) {
            e.printStackTrace();
            fail();
        }

    }
}