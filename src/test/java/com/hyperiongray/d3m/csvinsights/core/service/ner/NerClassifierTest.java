package com.hyperiongray.d3m.csvinsights.core.service.ner;

import com.google.common.collect.Lists;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.Optional;

import static org.junit.Assert.fail;

public class NerClassifierTest {

    private final NerClassifier nerClassifier = new NerClassifier();

    @Test
    public void resolveTextToSingleEntity1() {
        String sentence = "Charles Dickens wrote a book in Amazon, Brazil about the evolution of the Galapagos";
        try {
            Optional<EnrType> optionalEntity = nerClassifier.resolveTextToSingleEntity(sentence);
            Assert.assertEquals(EnrType.LOCATION, optionalEntity.get());
        } catch (IOException e) {
            e.printStackTrace();
            fail();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void resolveTextToSingleEntity2() {

        String sentence = "USA,1,United States,\"19,390,604\",\n" +
                "CHN,2,China,\"12,237,700\",\n" +
                "JPN,3,Japan,\"4,872,137\",\n" +
                "DEU,4,Germany,\"3,677,439\",\n" +
                "GBR,5,";

        try {
            Optional<EnrType> optionalEntity = nerClassifier.resolveTextToSingleEntity(sentence);
            Assert.assertEquals(EnrType.LOCATION, optionalEntity.get());
        } catch (IOException e) {
            e.printStackTrace();
            fail();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void resolveTextToSingleEntity3() {
        String sentence = Lists.newArrayList("Charles", "Wayne Municipal Airport", "Leesburg International Airport", "Rickenbacker Air National Guard Base", "Lebanon Municipal Airport").toString();

        try {
            Optional<EnrType> optionalEntity = nerClassifier.resolveTextToSingleEntity(sentence);
            Assert.assertEquals(EnrType.LOCATION, optionalEntity.get());
        } catch (IOException e) {
            e.printStackTrace();
            fail();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void resolveTextToSingleEntity4() {
        String sentence = Lists.newArrayList("Charles", "Wayne", "Leesburg International Airport", "John Rickenbacker", "Lebanon Municipal Airport").toString();

        try {
            Optional<EnrType> optionalEntity = nerClassifier.resolveTextToSingleEntity(sentence);
            Assert.assertEquals(EnrType.PERSON, optionalEntity.get());
        } catch (IOException e) {
            e.printStackTrace();
            fail();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            fail();
        }
    }


    @Test
    public void resolveTextToSingleEntityMoney() {
        String sentence = Lists.newArrayList("12/12/2019", "100$", "Dollar", "€", "1233USD", "544 ARS", "date 2019-09-09", "at 13:59").toString();

        try {
            Optional<EnrType> optionalEntity = nerClassifier.resolveTextToSingleEntity(sentence);
//            Assert.assertEquals("PERSON", optionalEntity.get());
        } catch (IOException e) {
            e.printStackTrace();
            fail();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            fail();
        }
    }


}