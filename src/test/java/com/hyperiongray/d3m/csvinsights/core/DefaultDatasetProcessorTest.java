package com.hyperiongray.d3m.csvinsights.core;

import com.hyperiongray.d3m.csvinsights.core.model.Dataset;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;

public class DefaultDatasetProcessorTest {

    private final DefaultDatasetProcessor defaultDatasetProcessor = new DefaultDatasetProcessor();

    @Test
    public void process2() {
        String path = "src/test/resources/tables/RTC 2018_Leeds.csv";
        Dataset dataset = defaultDatasetProcessor.process(new File(path).toPath());
        Assert.assertTrue(dataset.hasHeaders());
        Assert.assertEquals(1995, dataset.getRows());


    }
}