package com.hyperiongray.d3m.csvinsights.core.source.parser;

import com.hyperiongray.d3m.csvinsights.core.DefaultDatasetProcessor;
import com.hyperiongray.d3m.csvinsights.core.model.Dataset;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.MalformedURLException;
import java.net.URL;

import static org.junit.Assert.*;

public class CharacterUtilTest {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final DefaultDatasetProcessor defaultDatasetProcessor = new DefaultDatasetProcessor();


    @Test
    public void isPrintable() {

        Assert.assertTrue(CharacterUtil.isPrintable("simple case"));
        Assert.assertTrue(CharacterUtil.isPrintable("not that simple case |#¢¢#∞¢∞¢¬∞¬∞=)(/&%$·"));
        Assert.assertFalse(CharacterUtil.isPrintable(" \b"));
    }


    @Test
    public void testTsvFile() throws MalformedURLException {
        String url = "https://d3m-test.s3-us-west-2.amazonaws.com/downloaded-files/crawl-tasks/DSCVRY-025160a3-dc27-46a1-867f-5e63cf14cb9a/zenodo.org/bdc3bd65-fd51-4218-8210-30acae9a3148/filename-7dc37b20-42b8-11ea-8397-99608af9be27";
        Dataset dataset = defaultDatasetProcessor.process(new URL(url));
        System.out.println(dataset);

    }

    @Test(expected = IllegalArgumentException.class)
    public void testBinaryFile() throws MalformedURLException {
        String url ="https://d3m-test.s3-us-west-2.amazonaws.com/downloaded-files/crawl-tasks/DSCVRY-5dd20e6d-b385-4fa3-a601-d60fe421b8b2/catalog.data.gov/9f2d7224-c62d-47e3-b5e6-ccf13336b8f3/filename-da2fce40-7a80-11ea-8082-eb53fc7f14d5";
        Dataset dataset = defaultDatasetProcessor.process(new URL(url));
        System.out.println(dataset);

    }
}