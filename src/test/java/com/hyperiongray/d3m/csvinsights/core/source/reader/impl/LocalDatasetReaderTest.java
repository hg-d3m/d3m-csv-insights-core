package com.hyperiongray.d3m.csvinsights.core.source.reader.impl;

import com.hyperiongray.d3m.csvinsights.core.source.RawDataset;
import com.hyperiongray.d3m.csvinsights.core.source.reader.DatasetReaderConfig;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;

public class LocalDatasetReaderTest {

    @Test
    public void testRead() {
        String path = "src/test/resources/tables/filename-culture.csv";
        LocalDatasetReader localDatasetReader = new LocalDatasetReader(new File(path).toPath(), DatasetReaderConfig.getDefault());
        RawDataset rawDataset = localDatasetReader.readAsDatasetRaw();
        Assert.assertEquals(10, rawDataset.getHeaders().size());
        Assert.assertEquals(14, rawDataset.getRows().size());
        for (int i = 0; i < rawDataset.getRows().size(); i++) {
            for (int j = 0; j < rawDataset.getHeaders().size(); j++) {
                Assert.assertNotNull(rawDataset.getRows().get(i).get(j));
            }
        }
        System.out.println(rawDataset);
    }

    @Test
    public void testRead2() {
        String path = "src/test/resources/tables/Men+Soccer+RW2019+10+28.csv";
        LocalDatasetReader localDatasetReader = new LocalDatasetReader(new File(path).toPath(), DatasetReaderConfig.getDefault());
        RawDataset rawDataset = localDatasetReader.readAsDatasetRaw();
        Assert.assertEquals(4, rawDataset.getHeaders().size());

        Assert.assertEquals("Order", rawDataset.getHeaders().get(0));
        Assert.assertEquals("Team", rawDataset.getHeaders().get(1));
        Assert.assertEquals("Rating", rawDataset.getHeaders().get(2));
        Assert.assertEquals("Conference", rawDataset.getHeaders().get(3));

        Assert.assertEquals(420, rawDataset.getRows().size());
        for (int i = 0; i < rawDataset.getRows().size(); i++) {
            for (int j = 0; j < rawDataset.getHeaders().size(); j++) {
                Assert.assertNotNull(rawDataset.getRows().get(i).get(j));
            }
        }
        System.out.println(rawDataset);
    }

    @Test
    public void testReadGDP() {
        String path = "src/test/resources/tables/GDP.csv";
        LocalDatasetReader localDatasetReader = new LocalDatasetReader(new File(path).toPath(), DatasetReaderConfig.getDefault());
        RawDataset rawDataset = localDatasetReader.readAsDatasetRaw();
        Assert.assertEquals(4, rawDataset.getHeaders().size());

        Assert.assertEquals("Country", rawDataset.getHeaders().get(0));
        Assert.assertEquals("Ranking", rawDataset.getHeaders().get(1));
        Assert.assertEquals("Country2", rawDataset.getHeaders().get(2));
        Assert.assertEquals("GDP", rawDataset.getHeaders().get(3));

        Assert.assertEquals(202, rawDataset.getRows().size());
        for (int i = 0; i < rawDataset.getRows().size(); i++) {
            for (int j = 0; j < rawDataset.getHeaders().size(); j++) {
                Assert.assertNotNull(rawDataset.getRows().get(i).get(j));
            }
        }

        System.out.println(rawDataset);
    }

}