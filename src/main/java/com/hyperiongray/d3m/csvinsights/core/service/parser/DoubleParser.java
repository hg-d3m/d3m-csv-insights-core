package com.hyperiongray.d3m.csvinsights.core.service.parser;

import com.hyperiongray.d3m.csvinsights.core.model.PrimitiveFieldType;

import java.text.NumberFormat;
import java.util.Objects;
import java.util.Optional;

public class DoubleParser implements Parser<Double> {
    private final PrimitiveFieldType primitiveFieldType = PrimitiveFieldType.DOUBLE;

    private final NumberFormat numberFormat;

    public DoubleParser(NumberFormat numberFormat) {
        super();
        this.numberFormat = numberFormat;
    }

    public Optional<Double> parse(String input) {
        try {
            if (numberFormat == null) {
                return Optional.of(Double.parseDouble(input));
            } else {
                Number parse = numberFormat.parse(input);
                return Optional.of(parse.doubleValue());
            }
        } catch (Exception e) {
            //ignored
        }
        return Optional.empty();
    }

    @Override
    public PrimitiveFieldType getPrimitiveFieldType() {
        return primitiveFieldType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DoubleParser that = (DoubleParser) o;
        return primitiveFieldType == that.primitiveFieldType &&
                Objects.equals(numberFormat, that.numberFormat);
    }

    @Override
    public int hashCode() {
        return Objects.hash(primitiveFieldType, numberFormat);
    }

    @Override
    public String toString() {
        return "DoubleParser{" +
                "primitiveFieldType=" + primitiveFieldType +
                ", numberFormat=" + numberFormat +
                '}';
    }
}