package com.hyperiongray.d3m.csvinsights.core.service.parser;

import com.hyperiongray.d3m.csvinsights.core.model.PrimitiveFieldType;

import java.time.LocalTime;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TimeParser implements Parser<LocalTime> {
    private final PrimitiveFieldType primitiveFieldType = PrimitiveFieldType.TIME;
    private Pattern pattern;

    public TimeParser(Pattern pattern) {
        super();
        this.pattern = pattern;
    }


    @Override
    public Optional<LocalTime> parse(String input) {
        try {
            Matcher matcher = pattern.matcher(input);
            if (matcher.matches()) {
                LocalTime lt = LocalTime.parse(input);
                return Optional.of(lt);
            }
        } catch (Exception e) {
            return Optional.empty();
        }
        return Optional.empty();
    }


    @Override
    public PrimitiveFieldType getPrimitiveFieldType() {
        return primitiveFieldType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TimeParser that = (TimeParser) o;
        return primitiveFieldType == that.primitiveFieldType &&
                Objects.equals(pattern, that.pattern);
    }

    @Override
    public int hashCode() {
        return Objects.hash(primitiveFieldType, pattern);
    }

    @Override
    public String toString() {
        return "TimeParser{" +
                "primitiveFieldType=" + primitiveFieldType +
                ", pattern=" + pattern +
                '}';
    }
}
