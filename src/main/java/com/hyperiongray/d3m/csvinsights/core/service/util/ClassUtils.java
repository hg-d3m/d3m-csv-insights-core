package com.hyperiongray.d3m.csvinsights.core.service.util;

public class ClassUtils {

    public static <T> T convertInstanceOfObject(Object o, Class<T> clazz) {
        return clazz.cast(o);
    }

}
