package com.hyperiongray.d3m.csvinsights.core.model.column.impl;

import com.google.common.collect.Lists;
import com.hyperiongray.d3m.csvinsights.core.model.Field;
import com.hyperiongray.d3m.csvinsights.core.service.ner.EnrType;
import com.hyperiongray.d3m.csvinsights.core.service.stats.Summarizable;
import com.hyperiongray.d3m.csvinsights.core.service.stats.Summary;
import com.hyperiongray.d3m.csvinsights.core.service.stats.summarycalculator.impl.StringStatsCalculator;
import com.hyperiongray.d3m.csvinsights.core.model.column.Column;
import com.hyperiongray.d3m.csvinsights.core.service.ner.NerClassifier;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class StringColumn extends Column implements Summarizable {

    private final NerClassifier nerClassifier;
    private EnrType enrType;

    private List<String> data = Lists.newArrayList();

    public StringColumn(Field field, List<String> inputs, NerClassifier nerClassifier) {
        super(field);
        init(inputs);
        this.nerClassifier = nerClassifier;
        classify();
    }

    public void fill(List<String> inputs) {
        data = new ArrayList<>(inputs);
    }

    public List<String> getData() {
        return data;
    }


    public List<String> getSample(int size) {
        return data.stream().limit(size).collect(Collectors.toList());
    }

    @Override
    protected Summary calculateSummary() {
        return new StringStatsCalculator(data).getSummary();
    }


    private void classify() {
        String colAsText = String.join(", ", this.getSample(10));
        try {
            Optional<EnrType> optionalEntity = nerClassifier.resolveTextToSingleEntity(colAsText);
            optionalEntity.ifPresent(type -> enrType = type);
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    public EnrType getEnrType() {
        return enrType;
    }

}
