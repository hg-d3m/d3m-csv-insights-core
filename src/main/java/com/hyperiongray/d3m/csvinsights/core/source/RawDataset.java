package com.hyperiongray.d3m.csvinsights.core.source;

import java.util.List;

public class RawDataset {


    private final List<String> headers;
    private final List<List<String>> rows; // list of rows, each containing a list of values per cell

    private RawDataset(Builder builder) {
        headers = builder.headers;
        rows = builder.rows;
    }

    public int getFileWidth() {
        if (headers != null)
            return headers.size();
        else
            return rows.get(0).size();
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public List<String> getHeaders() {
        return headers;
    }

    public List<List<String>> getRows() {
        return rows;
    }

    public static final class Builder {
        private List<String> headers;
        private List<List<String>> rows;

        private Builder() {
        }

        public Builder withHeaders(List<String> headers) {
            this.headers = headers;
            return this;
        }

        public Builder withRows(List<List<String>> rows) {
            this.rows = rows;
            return this;
        }

        public RawDataset build() {
            return new RawDataset(this);
        }
    }

    @Override
    public String toString() {
        return "RawDataset{" +
                "headers=" + headers +
                ", rows=" + rows +
                '}';
    }
}
