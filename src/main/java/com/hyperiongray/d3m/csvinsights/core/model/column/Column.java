package com.hyperiongray.d3m.csvinsights.core.model.column;

import com.hyperiongray.d3m.csvinsights.core.model.Field;
import com.hyperiongray.d3m.csvinsights.core.model.Sample;
import com.hyperiongray.d3m.csvinsights.core.service.stats.Summary;
import com.hyperiongray.d3m.csvinsights.core.service.util.CollectionUtils;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public abstract class Column {

    private final Field field;
    private Summary summary;
    private Sample sample;

    public Column(Field field) {
        this.field = field;
    }

    protected void init(List<String> inputs) {
        fill(inputs);
        summary = calculateSummary();
        getSample(inputs);
    }

    private void getSample(List<String> inputs) {
        sample = new Sample();
        sample.setSample(inputs.stream().filter(x -> !x.isEmpty()).limit(10).collect(Collectors.toList()));

        Map<String, Long> frequencies = inputs.stream().collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        Map<String, Long> frequenciesSorted = CollectionUtils.sortByValueDesc(frequencies, 10);
        sample.setFrequencies(frequenciesSorted);
    }

    protected abstract void fill(List<String> inputs);

    protected abstract Summary calculateSummary();

    public Summary getSummary() {
        return summary;
    }

    public Field getField() {
        return field;
    }

    public Sample getSample() {
        return sample;
    }

    public void setSample(Sample sample) {
        this.sample = sample;
    }
    
    @Override
    public String toString() {
        return "Column{" +
                "field=" + field +
                ", summary=" + summary +
                ", sample=" + sample +
                '}';
    }
}
