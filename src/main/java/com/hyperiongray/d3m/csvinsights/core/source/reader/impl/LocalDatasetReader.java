package com.hyperiongray.d3m.csvinsights.core.source.reader.impl;

import com.hyperiongray.d3m.csvinsights.core.source.reader.DatasetReader;
import com.hyperiongray.d3m.csvinsights.core.source.reader.DatasetReaderConfig;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.nio.file.Path;


public class LocalDatasetReader extends DatasetReader {
    
    private final Path path;

    public LocalDatasetReader(Path path, DatasetReaderConfig datasetReaderConfig) {
        super(datasetReaderConfig);
        this.path = path;
    }

    @Override
    protected InputStream getInputStream() throws FileNotFoundException {
        return new FileInputStream(path.toAbsolutePath().toFile());
    }

/*
    @Override
    public String peak() {
        String line = Strings.EMPTY;
        try (InputStream stream = new FileInputStream(path.toAbsolutePath().toFile())) {
            LineIterator it = IOUtils.lineIterator(stream, StandardCharsets.UTF_8);
            if (it.hasNext()) {
                line = it.nextLine();
                line = trimBOM(line);
            }
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
        return line;
    }

    @Override
    public List<List<String>> readAll(String fieldsDelimiter, int limit) {
        List<List<String>> lines = Lists.newArrayList();
        boolean isFirstLine = true;
        try (InputStream stream = new FileInputStream(path.toAbsolutePath().toFile())) {
            LineIterator it = IOUtils.lineIterator(stream, StandardCharsets.UTF_8);
            while (it.hasNext() && lines.size() < limit) {
                String line = it.nextLine();
                if (isFirstLine) {
                    line = trimBOM(line);
                    isFirstLine = false;
                }
                List<String> fields = FieldParserUtil.parseLine(line, fieldsDelimiter.charAt(0));
                lines.add(fields);
            }

        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
        return lines;
    }*/

}
