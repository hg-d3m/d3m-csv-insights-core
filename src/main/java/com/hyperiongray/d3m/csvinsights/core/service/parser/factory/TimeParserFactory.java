package com.hyperiongray.d3m.csvinsights.core.service.parser.factory;

import com.google.common.collect.Lists;
import com.hyperiongray.d3m.csvinsights.core.service.parser.Parser;
import com.hyperiongray.d3m.csvinsights.core.service.parser.TimeParser;

import java.time.LocalTime;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TimeParserFactory implements ParserFactory<LocalTime> {

    private static final String REGEX_TIME = "(2[0-3]|[01]?[0-9]):?([0-5]?[0-9]):?([0-5]?[0-9])";
    private static final Pattern pattern = Pattern.compile(REGEX_TIME);

    @Override
    public List<Parser<LocalTime>> findParser(String input) {
        try {
            Matcher matcher = pattern.matcher(input);
            if (matcher.matches()) {
                LocalTime lt = LocalTime.parse(input);
                return Lists.newArrayList(new TimeParser(pattern));
            }
            return Lists.newArrayList();
        } catch (Exception e) {
            return Lists.newArrayList();
        }
    }
}
