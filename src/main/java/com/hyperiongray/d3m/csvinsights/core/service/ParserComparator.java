package com.hyperiongray.d3m.csvinsights.core.service;

import com.google.common.collect.Maps;
import com.hyperiongray.d3m.csvinsights.core.model.PrimitiveFieldType;
import com.hyperiongray.d3m.csvinsights.core.service.parser.Parser;

import java.util.Comparator;
import java.util.Map;

public class ParserComparator implements Comparator<Parser> {

    private final Map<PrimitiveFieldType, Integer> orderIndex = Maps.newHashMap();

    public ParserComparator() {
        orderIndex.put(PrimitiveFieldType.BOOLEAN, 1);
        orderIndex.put(PrimitiveFieldType.LONG, 2);
        orderIndex.put(PrimitiveFieldType.DOUBLE, 3);
        orderIndex.put(PrimitiveFieldType.DATETIME, 4);
        orderIndex.put(PrimitiveFieldType.DATE, 5);
        orderIndex.put(PrimitiveFieldType.TIME, 6);
        orderIndex.put(PrimitiveFieldType.STRING, 7);
    }

    @Override
    public int compare(Parser o1, Parser o2) {
        return orderIndex.get(o1.getPrimitiveFieldType()) -
                orderIndex.get(o2.getPrimitiveFieldType());
    }
}
