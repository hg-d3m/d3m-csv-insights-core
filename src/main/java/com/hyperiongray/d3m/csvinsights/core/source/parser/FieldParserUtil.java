package com.hyperiongray.d3m.csvinsights.core.source.parser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FieldParserUtil {

    private static String parseField(char[] chars, int startIndex, int endIndex) {
        String field = new String(Arrays.copyOfRange(chars, startIndex, endIndex));
        
        if(field.startsWith("\"") && field.endsWith("\"")) {
            field = field.substring(1, field.length()-1);
        }

        return field.replaceAll("\"\"", "\"").trim();
    }

    public static List<String> parseLine(String line, Character delimiter) {

        List<String> fields = new ArrayList<>();
        char[] chars = line.toCharArray();
        boolean quoteOpen = false;
        int start = 0;
        for (int i = 0; i < chars.length; i++) {
            char c = chars[i];
            if (c == '\"') {
                quoteOpen = !quoteOpen;
                continue;
            }
            if (!quoteOpen) {
                if (c == delimiter) {
                    fields.add(parseField(chars, start, i));
                    start = i + 1;
                }
            }
        }
        if (!quoteOpen) {
            fields.add(parseField(chars, start, chars.length));
        }
        return fields;
    }


}
