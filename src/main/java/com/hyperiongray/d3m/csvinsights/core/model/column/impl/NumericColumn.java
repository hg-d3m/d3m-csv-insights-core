package com.hyperiongray.d3m.csvinsights.core.model.column.impl;

import com.hyperiongray.d3m.csvinsights.core.model.Field;
import com.hyperiongray.d3m.csvinsights.core.model.column.Column;

public abstract class NumericColumn extends Column {
    public NumericColumn(Field field) {
        super(field);
    }
}
