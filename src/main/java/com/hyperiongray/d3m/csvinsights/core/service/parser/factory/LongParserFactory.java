package com.hyperiongray.d3m.csvinsights.core.service.parser.factory;

import com.google.common.collect.Lists;
import com.hyperiongray.d3m.csvinsights.core.service.parser.LongParser;
import com.hyperiongray.d3m.csvinsights.core.service.parser.Parser;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

public class LongParserFactory extends NumericParserFactory implements ParserFactory<Long> {

    @Override
    public List<Parser<Long>> findParser(String input) {
        try {
            Long v = Long.parseLong(input);
            return Lists.newArrayList(new LongParser());
        } catch (Exception e) {
            Optional<Locale> localeOptional = inferLocaleMillisSeparator(input);
            if (localeOptional.isPresent()) {
                Locale locale = localeOptional.get();
                try {
                    NumberFormat numberFormat = NumberFormat.getNumberInstance(locale);
                    numberFormat.setParseIntegerOnly(false);
                    Number parse = numberFormat.parse(input);
                    if (parse instanceof Long) {
                        return Lists.newArrayList(new LongParser(numberFormat));
                    }
                } catch (Exception e2) {
                    //ignore
                }
            }
        }
        return Lists.newArrayList();
    }

    protected Optional<Locale> inferLocaleMillisSeparator(String number) {

        if (number.indexOf('.') == -1) {
            if (number.indexOf(',') == -1) {
                return Optional.empty();
            } else if (number.lastIndexOf(',') == number.length() - 4) {
                return Optional.of(Locale.US);
            }
        } else if (number.lastIndexOf('.') == number.length() - 4) {
            return Optional.of(Locale.GERMANY);
        }
        return Optional.empty();
    }

}
