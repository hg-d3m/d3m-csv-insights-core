package com.hyperiongray.d3m.csvinsights.core.service.ner;

import edu.stanford.nlp.ie.AbstractSequenceClassifier;
import edu.stanford.nlp.ie.crf.CRFClassifier;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.DefaultPaths;
import edu.stanford.nlp.util.Triple;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.counting;

/*
// relevant links:
    https://nlp.stanford.edu/software/CRF-NER.shtml#Download
    https://github.com/stanfordnlp/CoreNLP
 */
public class NerClassifier {

    private AbstractSequenceClassifier<CoreLabel> classifier;

    public NerClassifier() {
        try {
            classifier = CRFClassifier.getClassifier(DefaultPaths.DEFAULT_NER_THREECLASS_MODEL);
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public Optional<EnrType> resolveTextToSingleEntity(String sentence) throws IOException, ClassNotFoundException {
        List<Triple<String, Integer, Integer>> triples = classify(sentence);
        return getMostLikelyEntity(triples);
    }

    private List<Triple<String, Integer, Integer>> classify(String sentence) {

        List<Triple<String, Integer, Integer>> triples = classifier.classifyToCharacterOffsets(sentence);
        for (Triple<String, Integer, Integer> trip : triples) {
            //System.out.printf("%s over character offsets [%d, %d) in sentence.%n", trip.first(), trip.second(), trip.third);
        }

        // This prints out all the details of what is stored for each token
        boolean debug = false;
        if (debug) {
            for (List<CoreLabel> lcl : classifier.classify(sentence)) {
                for (CoreLabel cl : lcl) {
                    System.out.println(cl.toShorterString());
                }
            }
        }

        return triples;
    }

    private static Optional<EnrType> getMostLikelyEntity(List<Triple<String, Integer, Integer>> triples) {
        Map<String, Long> map = triples.stream().collect(Collectors.groupingBy(x -> x.first, counting()));

        Map<String, Long> sortedMap = map.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));

        Optional<Map.Entry<String, Long>> first = sortedMap.entrySet().stream().findFirst();
        if(first.isPresent()){
            return Optional.of(EnrType.valueOf(first.get().getKey()));
        }
        return Optional.empty();
    }
}
