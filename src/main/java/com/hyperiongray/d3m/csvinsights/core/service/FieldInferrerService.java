package com.hyperiongray.d3m.csvinsights.core.service;

import com.hyperiongray.d3m.csvinsights.core.service.parser.Parser;
import com.hyperiongray.d3m.csvinsights.core.service.parser.StringParser;
import com.hyperiongray.d3m.csvinsights.core.service.parser.factory.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class FieldInferrerService {

    private final List<ParserFactory<?>> factories = new ArrayList<>();

    public FieldInferrerService() {
        factories.add(new BooleanParserFactory());
        factories.add(new LongParserFactory());
        factories.add(new DoubleParserFactory());
        factories.add(new TimeParserFactory());
        factories.add(new DateParserFactory());
        factories.add(new DateTimeParserFactory());
    }


    public Parser<?> infer(List<String> inputs) {
        List<Parser<?>> validParsers = new ArrayList<>();

        for (ParserFactory<?> parserFactory : factories) {
            inputs.forEach(input -> {
                List<? extends Parser<?>> parserOptional = parserFactory.findParser(input);
                validParsers.addAll(parserOptional);
            });
        }

        Map<Parser<?>, Long> grouped = validParsers.stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        if (grouped.size() == 0) {
            return new StringParser();
        }

        Long max = Collections.max(grouped.values());
        double strictness = inputs.size() * .9;
        double threshold = inputs.size() > 1 ? strictness : 1;

        if (max < threshold) {
            return new StringParser();
        }


        List<Parser<?>> grouped2 = grouped.entrySet().stream()
                .filter(e -> e.getValue().equals(max))
                .map(Map.Entry::getKey)
                .sorted(new ParserComparator())
                .collect(Collectors.toList());


        return grouped2.get(0);
    }

}
