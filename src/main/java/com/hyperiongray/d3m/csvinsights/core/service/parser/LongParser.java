package com.hyperiongray.d3m.csvinsights.core.service.parser;

import com.hyperiongray.d3m.csvinsights.core.model.PrimitiveFieldType;

import java.text.NumberFormat;
import java.util.Objects;
import java.util.Optional;

public class LongParser implements Parser<Long> {
    private final PrimitiveFieldType primitiveFieldType = PrimitiveFieldType.LONG;

    private NumberFormat numberFormat;

    public LongParser() {
    }

    public LongParser(NumberFormat numberFormat) {
        super();
        this.numberFormat = numberFormat;
    }

    public Optional<Long> parse(String input) {
        try {
            if (numberFormat == null) {
                return Optional.of(Long.parseLong(input));
            } else {
                Number parse = numberFormat.parse(input);
                return Optional.of(parse.longValue());
            }
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    @Override
    public PrimitiveFieldType getPrimitiveFieldType() {
        return primitiveFieldType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LongParser that = (LongParser) o;
        return primitiveFieldType == that.primitiveFieldType &&
                Objects.equals(numberFormat, that.numberFormat);
    }

    @Override
    public int hashCode() {
        return Objects.hash(primitiveFieldType, numberFormat);
    }

    @Override
    public String toString() {
        return "LongParser{" +
                "primitiveFieldType=" + primitiveFieldType +
                ", numberFormat=" + numberFormat +
                '}';
    }
}