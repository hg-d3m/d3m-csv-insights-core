package com.hyperiongray.d3m.csvinsights.core.model.column.impl;

import com.google.common.collect.Lists;
import com.hyperiongray.d3m.csvinsights.core.model.Field;
import com.hyperiongray.d3m.csvinsights.core.service.parser.TimeParser;
import com.hyperiongray.d3m.csvinsights.core.service.stats.Summarizable;
import com.hyperiongray.d3m.csvinsights.core.service.stats.Summary;
import com.hyperiongray.d3m.csvinsights.core.service.stats.summarycalculator.impl.GenericStatsCalculator;
import com.hyperiongray.d3m.csvinsights.core.model.column.Column;

import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

import static com.hyperiongray.d3m.csvinsights.core.service.util.ClassUtils.convertInstanceOfObject;

public class TimeColumn extends Column implements Summarizable {

    private final List<LocalTime> data = Lists.newArrayList();
    private TimeParser parser;


    public TimeColumn(Field field, List<String> inputs) {
        super(field);
        parser = convertInstanceOfObject(field.getParser(), TimeParser.class);
        init(inputs);
    }

    @Override
    protected void fill(List<String> inputs) {
        for (String input : inputs) {
            Optional<LocalTime> optional = parser.parse(input);
            data.add(optional.orElse(null));
        }
    }

    public List<LocalTime> getData() {
        return data;
    }

    @Override
    protected Summary calculateSummary() {
        return new GenericStatsCalculator<>(data).getSummary();
    }


}
