package com.hyperiongray.d3m.csvinsights.core.service.parser.factory;

import com.google.common.collect.Lists;
import com.hyperiongray.d3m.csvinsights.core.service.parser.BooleanParser;
import com.hyperiongray.d3m.csvinsights.core.service.parser.Parser;

import java.util.List;
import java.util.function.Predicate;

public class BooleanParserFactory implements ParserFactory<Boolean> {

    final Predicate<String> trueFalse = input -> input.equalsIgnoreCase("true") || input.equalsIgnoreCase("false");
    final Predicate<String> yesNo = input -> input.equalsIgnoreCase("yes") || input.equalsIgnoreCase("no");
    final Predicate<String> oneZero = input -> input.equalsIgnoreCase("1") || input.equalsIgnoreCase("0");

    @Override
    public List<Parser<Boolean>> findParser(String input) {
        if (trueFalse.test(input)) {
            return Lists.newArrayList(new BooleanParser(trueFalse));
        } else if (yesNo.test(input)) {
            return Lists.newArrayList(new BooleanParser(yesNo));
        } else if (oneZero.test(input)) {
            return Lists.newArrayList(new BooleanParser(oneZero));
        }
        return Lists.newArrayList();
    }
}
