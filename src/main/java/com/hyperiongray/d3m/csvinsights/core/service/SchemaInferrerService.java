package com.hyperiongray.d3m.csvinsights.core.service;

import com.hyperiongray.d3m.csvinsights.core.model.Field;
import com.hyperiongray.d3m.csvinsights.core.model.PrimitiveFieldType;
import com.hyperiongray.d3m.csvinsights.core.model.Schema;
import com.hyperiongray.d3m.csvinsights.core.service.parser.Parser;
import com.hyperiongray.d3m.csvinsights.core.source.RawDataset;

import java.util.List;
import java.util.stream.Collectors;


public class SchemaInferrerService {

    private final FieldInferrerService fieldInferrerService;

    public SchemaInferrerService(FieldInferrerService fieldInferrerService) {
        this.fieldInferrerService = fieldInferrerService;
    }

    public Schema inferSchema(RawDataset rawDataset, int limit) {

        List<List<String>> inputAsColumn = RowsToColumnsConverter.transformRowsToColumns(rawDataset);
        Schema schema = new Schema();
        boolean hasHeaders = false;
        for (int index = 0; index < inputAsColumn.size(); index++) {
            Field field = new Field();
            field.setColumnIndex(index);
            field.setName(rawDataset.getHeaders().get(index));

            Parser<?> headerInferredParser = fieldInferrerService.infer(inputAsColumn.get(index).subList(0, 1));

            List<String> sample = inputAsColumn.get(index).stream()
                    .skip(1) // starting from 1 to skip headers
                    .filter(x -> !x.isEmpty())
                    .limit(limit)
                    .collect(Collectors.toList());

            Parser<?> columnInferredParser = fieldInferrerService.infer(sample);
            if (headerInferredParser.getPrimitiveFieldType() == PrimitiveFieldType.STRING
                    && columnInferredParser.getPrimitiveFieldType() != PrimitiveFieldType.STRING) {
                hasHeaders = true;
            }
            field.setParser(columnInferredParser);
            schema.addField(field);
        }
        schema.setHeaders(hasHeaders);
        return schema;
    }


}
