package com.hyperiongray.d3m.csvinsights.core.service.parser;

import com.hyperiongray.d3m.csvinsights.core.model.PrimitiveFieldType;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.Objects;
import java.util.Optional;

public class DateParser implements Parser<LocalDate> {

    private final PrimitiveFieldType primitiveFieldType = PrimitiveFieldType.DATE;
    private final String patternFormat;

    public DateParser(String patternFormat) {
        this.patternFormat = patternFormat;
    }

    @Override
    public Optional<LocalDate> parse(String input) {
        try {
            TemporalAccessor dt = DateTimeFormatter.ofPattern(patternFormat).parse(input);
            return Optional.of(LocalDate.from(dt));
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    @Override
    public PrimitiveFieldType getPrimitiveFieldType() {
        return primitiveFieldType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DateParser that = (DateParser) o;
        return primitiveFieldType == that.primitiveFieldType &&
                Objects.equals(patternFormat, that.patternFormat);
    }

    @Override
    public int hashCode() {
        return Objects.hash(primitiveFieldType, patternFormat);
    }

    @Override
    public String toString() {
        return "DateParser{" +
                "primitiveFieldType=" + primitiveFieldType +
                ", patternFormat='" + patternFormat + '\'' +
                '}';
    }
}
