package com.hyperiongray.d3m.csvinsights.core.service.stats.summarycalculator.impl;

import com.google.common.math.Stats;
import com.hyperiongray.d3m.csvinsights.core.service.stats.summarycalculator.StatsCalculator;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.google.common.math.Quantiles.percentiles;

public class IntegerStatsCalculator extends StatsCalculator<BigInteger> {

    public IntegerStatsCalculator(List<BigInteger> list) {
        super(list);
    }

    public Optional<Stats> getMathStats() {
        return Optional.of(Stats.of(list));
    }

    public Map<Integer, Double> getDistribution() {
        return percentiles().indexes(25, 50, 75, 90, 99).compute(list);
    }


}
