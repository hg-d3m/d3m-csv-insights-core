package com.hyperiongray.d3m.csvinsights.core.api.dto;

public class MathStatsDto {

    private long count;
    private double mean;
    private double sumOfSquaresOfDeltas;
    private double min;
    private double max;

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public double getMean() {
        return mean;
    }

    public void setMean(double mean) {
        this.mean = mean;
    }

    public double getSumOfSquaresOfDeltas() {
        return sumOfSquaresOfDeltas;
    }

    public void setSumOfSquaresOfDeltas(double sumOfSquaresOfDeltas) {
        this.sumOfSquaresOfDeltas = sumOfSquaresOfDeltas;
    }

    public double getMin() {
        return min;
    }

    public void setMin(double min) {
        this.min = min;
    }

    public double getMax() {
        return max;
    }

    public void setMax(double max) {
        this.max = max;
    }
}
