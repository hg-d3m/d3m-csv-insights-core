package com.hyperiongray.d3m.csvinsights.core.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hyperiongray.d3m.csvinsights.core.model.column.Column;

import java.util.List;

public class Dataset {
    
    private final boolean headers;
    private final int rows;
    private final List<Column> columns;

    private Dataset(Builder builder) {
        rows = builder.rows;
        columns = builder.columns;
        headers = builder.headers;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public boolean hasHeaders() {
        return headers;
    }

    public int getRows() {
        return rows;
    }

    public List<Column> getColumns() {
        return columns;
    }

    public static final class Builder {
        private int rows;
        private List<Column> columns;
        private boolean headers;

        private Builder() {
        }

        public Builder withRows(int rows) {
            this.rows = rows;
            return this;
        }

        public Builder withColumns(List<Column> columns) {
            this.columns = columns;
            return this;
        }

        public Builder withHeaders(Boolean headers) {
            this.headers = headers;
            return this;
        }

        public Dataset build() {
            return new Dataset(this);
        }
    }

    @Override
    public String toString() {
        return "Dataset{" +
                "rows=" + rows +
                ", columns=" + columns +
                ", headers=" + headers +
                '}';
    }

    public String toJson() {
        try {
            return new ObjectMapper().writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return "";
        }
    }
}
