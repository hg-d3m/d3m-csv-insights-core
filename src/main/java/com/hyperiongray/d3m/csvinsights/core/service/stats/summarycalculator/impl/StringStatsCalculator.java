package com.hyperiongray.d3m.csvinsights.core.service.stats.summarycalculator.impl;

import com.google.common.collect.Ordering;
import com.hyperiongray.d3m.csvinsights.core.service.stats.summarycalculator.StatsCalculator;

import java.util.List;

public class StringStatsCalculator extends StatsCalculator<String> {

    public StringStatsCalculator(List<String> list) {
        super(list);
    }

    public Boolean isSorted() {
        return Ordering.natural().isOrdered(list);
    }

    public Boolean isStrictlySorted() {
        return Ordering.natural().isStrictlyOrdered(list);
    }

    public Boolean isReverseSorted() {
        return Ordering.natural().reverse().isOrdered(list);
    }


}
