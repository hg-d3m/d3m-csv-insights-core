package com.hyperiongray.d3m.csvinsights.core.service.parser.factory;

import com.google.common.collect.Lists;
import com.hyperiongray.d3m.csvinsights.core.service.parser.DoubleParser;
import com.hyperiongray.d3m.csvinsights.core.service.parser.Parser;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

public class DoubleParserFactory extends NumericParserFactory implements ParserFactory<Double> {

    @Override
    public List<Parser<Double>> findParser(String input) {
        try {
            Double v = Double.parseDouble(input);
            return Lists.newArrayList(new DoubleParser(null));
        } catch (Exception e) {
            Optional<Locale> localeOptional = inferLocaleDecimalSeparator(input);
            if (localeOptional.isPresent()) {
                Locale locale = localeOptional.get();
                try {
                    NumberFormat numberFormat = NumberFormat.getNumberInstance(locale);
                    numberFormat.setParseIntegerOnly(false);
                    Number parse = numberFormat.parse(input);
                    if (parse instanceof Double) {
                        return Lists.newArrayList(new DoubleParser(numberFormat));
                    }
                } catch (Exception e2) {
                    //ignore
                }
            }
        }
        return Lists.newArrayList();
    }

    protected Optional<Locale> inferLocaleDecimalSeparator(String number) {

        if (number.indexOf('.') == -1) {
            if (number.indexOf(',') == -1) {
                return Optional.empty();
            } else if (number.lastIndexOf(',') == number.length() - 3) {
                return Optional.of(Locale.GERMANY);
            }
        } else if (number.lastIndexOf('.') == number.length() - 3) {
            return Optional.of(Locale.US);
        }
        return Optional.empty();
    }

}
