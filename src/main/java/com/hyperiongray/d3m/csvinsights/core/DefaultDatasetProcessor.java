package com.hyperiongray.d3m.csvinsights.core;

import com.hyperiongray.d3m.csvinsights.core.api.DatasetProcessorAPI;
import com.hyperiongray.d3m.csvinsights.core.model.Dataset;
import com.hyperiongray.d3m.csvinsights.core.service.FieldInferrerService;
import com.hyperiongray.d3m.csvinsights.core.model.Schema;
import com.hyperiongray.d3m.csvinsights.core.service.DatasetBuilderService;
import com.hyperiongray.d3m.csvinsights.core.service.SchemaInferrerService;
import com.hyperiongray.d3m.csvinsights.core.service.exceptions.FailedToParseDatasetException;
import com.hyperiongray.d3m.csvinsights.core.service.ner.NerClassifier;
import com.hyperiongray.d3m.csvinsights.core.source.RawDataset;
import com.hyperiongray.d3m.csvinsights.core.source.RawDatasets;
import com.hyperiongray.d3m.csvinsights.core.source.reader.DatasetReaderConfig;

import java.net.URL;
import java.nio.file.Path;

public class DefaultDatasetProcessor implements DatasetProcessorAPI {

    private final FieldInferrerService fieldInferrerService = new FieldInferrerService();
    private final SchemaInferrerService schemaInferrerService = new SchemaInferrerService(fieldInferrerService);

    private final NerClassifier nerClassifier = new NerClassifier();
    private final DatasetBuilderService datasetBuilderService = new DatasetBuilderService(nerClassifier);

    @Override
    public Dataset process(URL datasetUrl) {
        DatasetReaderConfig datasetReaderConfig = DatasetReaderConfig.getDefault();
        RawDataset rawDataset = RawDatasets.from(datasetUrl, datasetReaderConfig);
        return getDataset(datasetReaderConfig, rawDataset);
    }

    @Override
    public Dataset process(URL datasetUrl, DatasetReaderConfig datasetReaderConfig) {
        RawDataset rawDataset = RawDatasets.from(datasetUrl, datasetReaderConfig);
        return getDataset(datasetReaderConfig, rawDataset);
    }

    @Override
    public Dataset process(Path path) {
        DatasetReaderConfig datasetReaderConfig = DatasetReaderConfig.getDefault();
        RawDataset rawDataset = RawDatasets.from(path, datasetReaderConfig);
        return getDataset(datasetReaderConfig, rawDataset);
    }

    @Override
    public Dataset process(Path path, DatasetReaderConfig datasetReaderConfig) {
        RawDataset rawDataset = RawDatasets.from(path, datasetReaderConfig);
        return getDataset(datasetReaderConfig, rawDataset);
    }

    private Dataset getDataset(DatasetReaderConfig datasetReaderConfig, RawDataset rawDataset) {
        try {
            Schema schema = schemaInferrerService.inferSchema(rawDataset, datasetReaderConfig.getSampleLimitForSchemaInferring());
            return datasetBuilderService.build(rawDataset, schema);
        } catch (Exception e) {
            System.out.println(e);
            throw new FailedToParseDatasetException("Failed to parse");
        }
    }

}
