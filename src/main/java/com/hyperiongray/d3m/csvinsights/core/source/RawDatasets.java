package com.hyperiongray.d3m.csvinsights.core.source;


import com.hyperiongray.d3m.csvinsights.core.source.reader.DatasetReaderConfig;
import com.hyperiongray.d3m.csvinsights.core.source.reader.impl.LocalDatasetReader;
import com.hyperiongray.d3m.csvinsights.core.source.reader.impl.RemoteDatasetReader;

import java.net.URL;
import java.nio.file.Path;

public class RawDatasets {

    public static RawDataset from(URL url, DatasetReaderConfig datasetReaderConfig) {
        RemoteDatasetReader remoteDatasetReader = new RemoteDatasetReader(url, datasetReaderConfig);
        return remoteDatasetReader.readAsDatasetRaw();
    }

    //Path path = Path.of(absolutePath);
    public static RawDataset from(Path path, DatasetReaderConfig datasetReaderConfig) {
        return new LocalDatasetReader(path, datasetReaderConfig).readAsDatasetRaw();
    }

}
