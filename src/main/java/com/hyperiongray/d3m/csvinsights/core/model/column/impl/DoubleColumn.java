package com.hyperiongray.d3m.csvinsights.core.model.column.impl;

import com.google.common.collect.Lists;
import com.hyperiongray.d3m.csvinsights.core.model.Field;
import com.hyperiongray.d3m.csvinsights.core.model.column.Column;
import com.hyperiongray.d3m.csvinsights.core.service.parser.DoubleParser;
import com.hyperiongray.d3m.csvinsights.core.service.stats.Summarizable;
import com.hyperiongray.d3m.csvinsights.core.service.stats.Summary;
import com.hyperiongray.d3m.csvinsights.core.service.stats.summarycalculator.impl.NumericStatsCalculator;
import com.hyperiongray.d3m.csvinsights.core.service.util.ClassUtils;

import java.util.List;
import java.util.Optional;

public class DoubleColumn extends Column implements Summarizable {

    private final List<Double> data = Lists.newArrayList();
    private DoubleParser parser;

    public DoubleColumn(Field field, List<String> inputs) {
        super(field);
        parser = ClassUtils.convertInstanceOfObject(field.getParser(), DoubleParser.class);
        init(inputs);
    }

    protected void fill(List<String> inputs) {
        for (String input : inputs) {
            Optional<Double> optional = parser.parse(input);
            data.add(optional.orElse(null));
        }
    }

    public List<Double> getData() {
        return data;
    }

    @Override
    protected Summary calculateSummary() {
        return new NumericStatsCalculator(data).getSummary();
    }
}
