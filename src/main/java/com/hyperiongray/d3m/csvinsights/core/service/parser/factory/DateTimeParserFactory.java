package com.hyperiongray.d3m.csvinsights.core.service.parser.factory;

import com.google.common.collect.Lists;
import com.hyperiongray.d3m.csvinsights.core.service.parser.DateTimeParser;
import com.hyperiongray.d3m.csvinsights.core.service.parser.Parser;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class DateTimeParserFactory implements ParserFactory<ZonedDateTime> {


    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd['T'][' ']HH:mm:ss[.SSS]['Z']");

    @Override
    public List<Parser<ZonedDateTime>> findParser(String input) {
        try {
            ZonedDateTime utc = LocalDateTime.parse(input, formatter).atZone(ZoneId.of("UTC"));
            return Lists.newArrayList(new DateTimeParser(formatter));
        } catch (Exception e) {
            return Lists.newArrayList();
        }
    }
}
