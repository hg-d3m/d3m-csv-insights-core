package com.hyperiongray.d3m.csvinsights.core.service.stats.summarycalculator.impl;

import com.hyperiongray.d3m.csvinsights.core.service.stats.summarycalculator.StatsCalculator;

import java.util.List;

public class BooleanStatsCalculator extends StatsCalculator<Boolean> {

    public BooleanStatsCalculator(List<Boolean> list) {
        super(list);
    }

    public Boolean isUnique() {
        return false;
    }

    public Boolean isPkCandidate() {
        return false;
    }

}
