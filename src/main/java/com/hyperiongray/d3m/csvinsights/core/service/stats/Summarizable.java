package com.hyperiongray.d3m.csvinsights.core.service.stats;

public interface Summarizable {

    Summary getSummary();
}
