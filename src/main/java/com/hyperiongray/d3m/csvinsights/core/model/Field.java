package com.hyperiongray.d3m.csvinsights.core.model;

import com.hyperiongray.d3m.csvinsights.core.service.parser.Parser;

public class Field {
    private int columnIndex;
    private String name;
    private Parser<?> parser;

    public int getColumnIndex() {
        return columnIndex;
    }

    public void setColumnIndex(int columnIndex) {
        this.columnIndex = columnIndex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public String toString() {
        return "Field{" +
                "columnIndex=" + columnIndex +
                ", name='" + name + '\'' +
                ", parser=" + parser +
                '}';
    }

    public Parser<?> getParser() {
        return parser;
    }

    public void setParser(Parser<?> parser) {
        this.parser = parser;
    }
}
