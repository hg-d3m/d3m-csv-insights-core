package com.hyperiongray.d3m.csvinsights.core.service.stats;

import java.util.HashMap;
import java.util.Map;

public class Summary {

    private int count;
    private int nulls;
    private int notNulls;
    private int distinct;

    private boolean isNullable;
    private boolean isUnique;
    private boolean isPrimaryKeyCandidate;


    private Map<Integer, Double> distribution = new HashMap<>();

    private Summary(Builder builder) {
        count = builder.count;
        nulls = builder.nulls;
        notNulls = builder.notNulls;
        distinct = builder.distinct;
        isNullable = builder.isNullable;
        isUnique = builder.isUnique;
        isPrimaryKeyCandidate = builder.isPrimaryKeyCandidate;
        distribution = builder.distribution;
    }


    public int getCount() {
        return count;
    }

    public int getNulls() {
        return nulls;
    }

    public int getNotNulls() {
        return notNulls;
    }

    public int getDistinct() {
        return distinct;
    }

    public boolean isNullable() {
        return isNullable;
    }

    public boolean isUnique() {
        return isUnique;
    }

    public boolean isPrimaryKeyCandidate() {
        return isPrimaryKeyCandidate;
    }

    public Map<Integer, Double> getDistribution() {
        return distribution;
    }

    public static Builder newBuilder() {
        return new Builder();
    }


    public static final class Builder {
        private int count;
        private int nulls;
        private int notNulls;
        private int distinct;
        private boolean isNullable;
        private boolean isUnique;
        private boolean isPrimaryKeyCandidate;
        private Map<Integer, Double> distribution;

        private Builder() {
        }

        public Builder withCount(int count) {
            this.count = count;
            return this;
        }

        public Builder withNulls(int nulls) {
            this.nulls = nulls;
            return this;
        }

        public Builder withNotNulls(int notNulls) {
            this.notNulls = notNulls;
            return this;
        }

        public Builder withDistinct(int distinct) {
            this.distinct = distinct;
            return this;
        }

        public Builder withIsNullable(boolean isNullable) {
            this.isNullable = isNullable;
            return this;
        }

        public Builder withIsUnique(boolean isUnique) {
            this.isUnique = isUnique;
            return this;
        }

        public Builder withIsPrimaryKeyCandidate(boolean isPrimaryKeyCandidate) {
            this.isPrimaryKeyCandidate = isPrimaryKeyCandidate;
            return this;
        }

        public Builder withDistribution(Map<Integer, Double> distribution) {
            this.distribution = distribution;
            return this;
        }

        public Summary build() {
            return new Summary(this);
        }
    }

    @Override
    public String toString() {
        return "Summary{" +
                "count=" + count +
                ", nulls=" + nulls +
                ", notNulls=" + notNulls +
                ", distinct=" + distinct +
                ", isNullable=" + isNullable +
                ", isUnique=" + isUnique +
                ", isPrimaryKeyCandidate=" + isPrimaryKeyCandidate +
                ", distribution=" + distribution +
                '}';
    }
}



    /*
        private T first;
        private List<T> frequentItems;
    */

//Map<String, Integer> getCountStats();
    /*public Map<String, Integer> getCountStats() {
        Map<String, Integer> stats = Maps.newLinkedHashMap();
        stats.put("count", getCount());
        stats.put("nulls", getNullsCount());
        stats.put("notNulls", getNotNullCount());
        stats.put("distinct", getDistinct());
        return stats;
    }
*/

//    Optional<Stats> getMathStats();
