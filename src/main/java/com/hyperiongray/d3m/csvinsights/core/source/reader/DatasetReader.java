package com.hyperiongray.d3m.csvinsights.core.source.reader;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.hyperiongray.d3m.csvinsights.core.source.RawDataset;
import com.hyperiongray.d3m.csvinsights.core.source.parser.FieldDelimiterUtil;
import com.hyperiongray.d3m.csvinsights.core.source.parser.FieldParserUtil;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.LineIterator;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Optional;

import static com.hyperiongray.d3m.csvinsights.core.source.parser.BomHelper.trimBOM;
import static com.hyperiongray.d3m.csvinsights.core.source.parser.CharacterUtil.isPrintable;

public abstract class DatasetReader {

    private final DatasetReaderConfig datasetReaderConfig;

    public DatasetReader(DatasetReaderConfig datasetReaderConfig) {
        this.datasetReaderConfig = datasetReaderConfig;
    }

    protected abstract InputStream getInputStream() throws IOException;

    public DatasetReaderConfig getDatasetReaderConfig() {
        return datasetReaderConfig;
    }

    protected String peak() {
        String line = "";
        try (InputStream stream = getInputStream()) {
            LineIterator it = IOUtils.lineIterator(stream, StandardCharsets.UTF_8);
            while (Strings.isNullOrEmpty(line)) {
                if (it.hasNext()) {
                    line = it.nextLine();
                    line = trimBOM(line);

                    if(!isPrintable(line)){
                        throw new IllegalArgumentException("File is binary");
                    }

                } else {
                    throw new IllegalArgumentException("File is empty");
                }
            }

        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
        return line;
    }

    protected List<List<String>> readAll(String fieldsDelimiter, int limit) {
        List<List<String>> lines = Lists.newArrayList();
        boolean isFirstLine = true;
        try (InputStream stream = getInputStream()) {
            LineIterator it = IOUtils.lineIterator(stream, StandardCharsets.UTF_8);
            while (it.hasNext() && lines.size() < limit) {
                String line = it.nextLine();
                if (isFirstLine) {
                    line = trimBOM(line);
                    isFirstLine = false;
                }
                List<String> fields = FieldParserUtil.parseLine(line, fieldsDelimiter.charAt(0));
                lines.add(fields);
            }

        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
        return lines;
    }


    public RawDataset readAsDatasetRaw() {
        String firstLine = peak();
        if (Strings.isNullOrEmpty(firstLine)) {
            throw new IllegalArgumentException("File is empty");
        }
        if (firstLine.trim().toLowerCase().startsWith("<!doctype") || firstLine.trim().toLowerCase().startsWith("<html")) {
            throw new IllegalArgumentException("File is HTML");
        }

        String delimiter = this.datasetReaderConfig.getFieldsDelimiter() == null ? getDelimiter(firstLine) : this.datasetReaderConfig.getFieldsDelimiter();

        List<String> headers = FieldParserUtil.parseLine(firstLine, delimiter.charAt(0));
        List<List<String>> rows = readAll(delimiter, datasetReaderConfig.getLimit());

        return RawDataset.newBuilder()
                .withHeaders(headers)
                .withRows(rows)
                .build();
    }

    private String getDelimiter(String firstLine) {
        String delimiter = datasetReaderConfig.getFieldsDelimiter();
        if (delimiter == null) {
            Optional<Character> optionalDelimiter = FieldDelimiterUtil.inferDelimiterFromLine(firstLine);
            delimiter = optionalDelimiter.map(Object::toString).orElse("*UNKNOWN*");
        }
        return delimiter;
    }

}
