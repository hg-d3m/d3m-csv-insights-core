package com.hyperiongray.d3m.csvinsights.core.service.stats.summarycalculator.impl;

import com.hyperiongray.d3m.csvinsights.core.service.stats.summarycalculator.StatsCalculator;

import java.time.ZonedDateTime;
import java.util.List;

public class DatetimeStatsCalculator extends StatsCalculator<ZonedDateTime> {
    
    public DatetimeStatsCalculator(List<ZonedDateTime> list) {
        super(list);
    }

    public Boolean isPkCandidate() {
        return false;
    }

}