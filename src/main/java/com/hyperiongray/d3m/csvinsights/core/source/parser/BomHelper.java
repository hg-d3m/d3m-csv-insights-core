package com.hyperiongray.d3m.csvinsights.core.source.parser;


import com.google.common.base.Strings;

public class BomHelper {
    public static final String UTF16_BOM = "\uFEFF";
    public static final String UTF8_BOM = "\u00ef\u00bb\u00bf";
    public static final String UTF8_WST = "\u200B";

    public static String trimBOM(String input) {
        if (Strings.isNullOrEmpty(input)) {
            return input;
        }
        if (input.startsWith(UTF16_BOM)) {
            input = input.substring(1);
            return trimBOM(input);
        } else if (input.startsWith(UTF8_BOM)) {
            input = input.substring(3);
            return trimBOM(input);
        } else if (input.startsWith(UTF8_WST)) {
            input = input.substring(1);
            return trimBOM(input);
        }

        return input;
    }
}
