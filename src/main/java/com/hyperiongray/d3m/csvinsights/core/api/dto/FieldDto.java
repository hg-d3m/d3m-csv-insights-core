package com.hyperiongray.d3m.csvinsights.core.api.dto;

import java.util.List;
import java.util.Map;

public class FieldDto {

    private int index;
    private String name;
    private String type;

    private CountStatsDto stats;

    private Boolean nullable;
    private Boolean unique;
    private Boolean pkCandidate;

    private MathStatsDto mathStats;

    private Map<Integer, Double> distribution;

    private String first;
    private List<String> frequent;
    private String enr;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public CountStatsDto getStats() {
        return stats;
    }

    public void setStats(CountStatsDto stats) {
        this.stats = stats;
    }

    public Boolean getNullable() {
        return nullable;
    }

    public void setNullable(Boolean nullable) {
        this.nullable = nullable;
    }

    public Boolean getUnique() {
        return unique;
    }

    public void setUnique(Boolean unique) {
        this.unique = unique;
    }

    public Boolean getPkCandidate() {
        return pkCandidate;
    }

    public void setPkCandidate(Boolean pkCandidate) {
        this.pkCandidate = pkCandidate;
    }

    public MathStatsDto getMathStats() {
        return mathStats;
    }

    public void setMathStats(MathStatsDto mathStats) {
        this.mathStats = mathStats;
    }

    public Map<Integer, Double> getDistribution() {
        return distribution;
    }

    public void setDistribution(Map<Integer, Double> distribution) {
        this.distribution = distribution;
    }

    public String getFirst() {
        return first;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    public List<String> getFrequent() {
        return frequent;
    }

    public void setFrequent(List<String> frequent) {
        this.frequent = frequent;
    }

    public String getEnr() {
        return enr;
    }

    public void setEnr(String enr) {
        this.enr = enr;
    }
    
    @Override
    public String toString() {
        return "FieldDto{" +
                "index=" + index +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", stats=" + stats +
                ", nullable=" + nullable +
                ", unique=" + unique +
                ", pkCandidate=" + pkCandidate +
                ", mathStats=" + mathStats +
                ", distribution=" + distribution +
                ", first='" + first + '\'' +
                ", frequent=" + frequent +
                '}';
    }
}


/*
    public JSONObject toJson() {

        JSONObject jsonObjectField = new JSONObject();
        jsonObjectField.put("index", columnIndex);
        jsonObjectField.put("name", field.getName());
        jsonObjectField.put("type", field.getType());
        jsonObjectField.put("format", field.getFormat());

        jsonObjectField.put("countStats", calculator.getCountStats());

        jsonObjectField.put("nullable", calculator.isNullable());
        jsonObjectField.put("unique", calculator.isUnique());
        jsonObjectField.put("pkCandidate", calculator.isPkCandidate());

        if (calculator.getMathStats().isPresent()) {
            jsonObjectField.put("mathStats", calculator.getMathStats().get());
        }
        if (calculator.getDistribution().size() > 0) {
            jsonObjectField.put("distribution", calculator.getDistribution());
        }
        if (calculator.getFirst().isPresent()) {
            jsonObjectField.put("first", calculator.getFirst().get());
        }
        jsonObjectField.put("frequent", calculator.getFrequentItems(5));

        return jsonObjectField;
    }

 */