package com.hyperiongray.d3m.csvinsights.core.service.parser;

import com.hyperiongray.d3m.csvinsights.core.model.PrimitiveFieldType;

import java.util.Optional;

public interface Parser<T> {

    Optional<T> parse(String input);

    PrimitiveFieldType getPrimitiveFieldType();
}
