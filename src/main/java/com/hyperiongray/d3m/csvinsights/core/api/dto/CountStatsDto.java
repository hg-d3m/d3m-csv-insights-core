package com.hyperiongray.d3m.csvinsights.core.api.dto;

public class CountStatsDto {
    private Integer count;
    private Integer nulls;
    private Integer notNulls;
    private Integer distinct;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getNulls() {
        return nulls;
    }

    public void setNulls(Integer nulls) {
        this.nulls = nulls;
    }

    public Integer getNotNulls() {
        return notNulls;
    }

    public void setNotNulls(Integer notNulls) {
        this.notNulls = notNulls;
    }

    public Integer getDistinct() {
        return distinct;
    }

    public void setDistinct(Integer distinct) {
        this.distinct = distinct;
    }

    @Override
    public String toString() {
        return "CountStatsDto{" +
                "count=" + count +
                ", nulls=" + nulls +
                ", notNulls=" + notNulls +
                ", distinct=" + distinct +
                '}';
    }
}



/*
    public Map<String, Integer> getCountStats() {
        Map<String, Integer> stats = Maps.newLinkedHashMap();
        stats.put("count", getCount());
        stats.put("nulls", getNullsCount());
        stats.put("notNulls", getNotNullCount());
        stats.put("distinct", getDistinct());
        return stats;
    }
*/
