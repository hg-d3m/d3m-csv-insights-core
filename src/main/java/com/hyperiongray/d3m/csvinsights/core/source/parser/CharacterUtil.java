package com.hyperiongray.d3m.csvinsights.core.source.parser;

import java.awt.event.KeyEvent;

public class CharacterUtil {

    public static boolean isPrintable(String input){
        float printable =0.001f;
        float nonPrintable =0.001f;
        for(char c :input.toCharArray()){
            if(c=='\n' || c=='\t' || c=='\'' || c=='\"'){
                continue;
            }
            if (isPrintableChar(c)) {
                printable++;
            } else {
                nonPrintable++;
            }
        }
        System.out.println(input);
        return nonPrintable/printable < 0.01;
    }


    private static boolean isPrintableChar(char c ) {
        Character.UnicodeBlock block = Character.UnicodeBlock.of( c );
        return (!Character.isISOControl(c)) &&
                c != KeyEvent.CHAR_UNDEFINED &&
                block != null &&
                block != Character.UnicodeBlock.SPECIALS;
    }

}
