package com.hyperiongray.d3m.csvinsights.core.model;

import java.util.ArrayList;
import java.util.List;

public class Schema {

    private boolean headers;
    private List<Field> fields = new ArrayList<>();

    public boolean hasHeaders() {
        return headers;
    }

    public void setHeaders(boolean headers) {
        this.headers = headers;
    }

    public List<Field> getFields() {
        return fields;
    }

    public void setFields(List<Field> fields) {
        this.fields = fields;
    }

    public void addField(Field field) {
        this.fields.add(field);
    }

    @Override
    public String toString() {
        return "Schema{" +
                "headers=" + headers +
                ", fields=" + fields +
                '}';
    }

}
