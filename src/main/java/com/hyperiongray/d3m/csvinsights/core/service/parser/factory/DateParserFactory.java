package com.hyperiongray.d3m.csvinsights.core.service.parser.factory;

import com.google.common.collect.Lists;
import com.hyperiongray.d3m.csvinsights.core.service.parser.DateParser;
import com.hyperiongray.d3m.csvinsights.core.service.parser.Parser;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.List;

public class DateParserFactory implements ParserFactory<LocalDate> {

    private static final List<String> patternFormats = Lists.newArrayList(
            "yyyy-MM-dd",
            "yyMMdd",
            "yyyyMMdd",
            "dd/MM/yy",
            "dd/MM/yyyy",
            "MM/dd/yy",
            "MM/dd/yyyy",
            "dd-MM-yyyy",
            "MM-dd-yyyy"
    );

    @Override
    public List<Parser<LocalDate>> findParser(String input) {
        List<Parser<LocalDate>> dateParsers = Lists.newArrayList();
        for (String patternFormat : patternFormats) {
            try {
                TemporalAccessor dt = DateTimeFormatter.ofPattern(patternFormat).parse(input);
                LocalDate from = LocalDate.from(dt);
                dateParsers.add(new DateParser(patternFormat));
            } catch (Exception e) {
                //ignored
            }

        }
        return dateParsers;
    }
}
