package com.hyperiongray.d3m.csvinsights.core.source.reader;

public class DatasetReaderConfig {
    private final static int URL_MAX_CONTENT_SIZE = 50 * 1024 * 1024;

    private int limit = Integer.MAX_VALUE;
    private final int sampleLimitForSchemaInferring;
    private final boolean strict;
    private final String fieldsDelimiter;
    private final long maxUrlContentSize;


    private DatasetReaderConfig(Builder builder) {
        this.limit = builder.limit == 0 ? Integer.MAX_VALUE : builder.limit;
        sampleLimitForSchemaInferring = builder.sampleLimitForSchemaInferring;
        strict = builder.strict;
        fieldsDelimiter = builder.fieldsDelimiter;
        maxUrlContentSize = builder.maxUrlContentSize == 0 ? URL_MAX_CONTENT_SIZE : builder.maxUrlContentSize;

    }

    public static Builder newBuilder() {
        return new Builder();
    }

    //Keep this
    public static DatasetReaderConfig getDefault() {
        return newBuilder().withStrict(false).withSampleLimitForSchemaInferring(50).build();
    }


    public int getLimit() {
        return limit;
    }

    public int getSampleLimitForSchemaInferring() {
        return sampleLimitForSchemaInferring;
    }

    public boolean isStrict() {
        return strict;
    }

    public long getMaxUrlContentSize() {
        return maxUrlContentSize;
    }

    public String getFieldsDelimiter() {
        return fieldsDelimiter;
    }

    public static final class Builder {
        private int limit;
        private int sampleLimitForSchemaInferring;
        private boolean strict;
        private String fieldsDelimiter;
        private long maxUrlContentSize;

        private Builder() {
        }

        public Builder withLimit(int limit) {
            this.limit = limit;
            return this;
        }

        public Builder withSampleLimitForSchemaInferring(int sampleLimitForSchemaInferring) {
            this.sampleLimitForSchemaInferring = sampleLimitForSchemaInferring;
            return this;
        }

        public Builder withStrict(boolean strict) {
            this.strict = strict;
            return this;
        }

        public Builder withFieldsDelimiter(String fieldsDelimiter) {
            this.fieldsDelimiter = fieldsDelimiter;
            return this;
        }

        public Builder withMaxUrlContentSize(long maxUrlContentSize) {
            this.maxUrlContentSize = maxUrlContentSize;
            return this;
        }

        public DatasetReaderConfig build() {
            return new DatasetReaderConfig(this);
        }
    }
}
