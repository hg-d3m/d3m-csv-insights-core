package com.hyperiongray.d3m.csvinsights.core.service.stats.summarycalculator.impl;

import com.google.common.math.Stats;
import com.hyperiongray.d3m.csvinsights.core.service.stats.summarycalculator.StatsCalculator;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.google.common.math.Quantiles.percentiles;

public class NumericStatsCalculator extends StatsCalculator<Number> {

    public NumericStatsCalculator(List<? extends Number> list) {
        super(list);
    }

    public Optional<Stats> getMathStats() {
        return Optional.of(Stats.of(list));
    }

    public Map<Integer, Double> getDistribution() {
        return percentiles().indexes(25, 50, 75, 90, 99).compute(list.stream().filter(x -> x != null).collect(Collectors.toList()));
    }




/*
    public Boolean isStrictlySorted(){
        return Ordering.natural().isStrictlyOrdered(list);
    }

    public Boolean isReverseSorted(){
        return Ordering.natural().reverse().isOrdered(list);
    }
*/
}
