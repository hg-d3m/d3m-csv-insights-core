package com.hyperiongray.d3m.csvinsights.core.model;

public enum PrimitiveFieldType {
    LONG, DOUBLE, DATETIME, DATE, TIME, BOOLEAN, STRING;
}
