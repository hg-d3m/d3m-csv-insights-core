package com.hyperiongray.d3m.csvinsights.core.model.column.impl;

import com.google.common.collect.Lists;
import com.hyperiongray.d3m.csvinsights.core.model.Field;
import com.hyperiongray.d3m.csvinsights.core.service.stats.Summarizable;
import com.hyperiongray.d3m.csvinsights.core.service.stats.Summary;
import com.hyperiongray.d3m.csvinsights.core.service.stats.summarycalculator.impl.DatetimeStatsCalculator;
import com.hyperiongray.d3m.csvinsights.core.model.column.Column;
import com.hyperiongray.d3m.csvinsights.core.service.parser.DateTimeParser;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

import static com.hyperiongray.d3m.csvinsights.core.service.util.ClassUtils.convertInstanceOfObject;

public class DateTimeColumn extends Column implements Summarizable {

    private final List<ZonedDateTime> data = Lists.newArrayList();
    private final DateTimeParser parser;

    public DateTimeColumn(Field field, List<String> inputs) {
        super(field);
        parser = convertInstanceOfObject(field.getParser(), DateTimeParser.class);
        init(inputs);
    }

    protected void fill(List<String> inputs) {
        for (String input : inputs) {
            Optional<ZonedDateTime> optional = parser.parse(input);
            data.add(optional.orElse(null));
        }
    }

    public List<ZonedDateTime> getData() {
        return data;
    }

    @Override
    protected Summary calculateSummary() {
        return new DatetimeStatsCalculator(data).getSummary();
    }

}
