package com.hyperiongray.d3m.csvinsights.core.model;

import com.google.common.collect.Maps;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Sample {

    private List<String> sample = new ArrayList<>();
    private Map<String, Long> frequencies = Maps.newHashMap();

    public List<String> getSample() {
        return sample;
    }

    public void setSample(List<String> sample) {
        this.sample = sample;
    }

    public Map<String, Long> getFrequent() {
        return frequencies;
    }

    public void setFrequencies(Map<String, Long> frequencies) {
        this.frequencies = frequencies;
    }

    @Override
    public String toString() {
        return "Sample{" +
                "sample=" + sample +
                ", frequencies=" + frequencies +
                '}';
    }


}
