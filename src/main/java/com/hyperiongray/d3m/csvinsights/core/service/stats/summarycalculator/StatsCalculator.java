package com.hyperiongray.d3m.csvinsights.core.service.stats.summarycalculator;

import com.google.common.collect.Maps;
import com.google.common.math.Stats;
import com.hyperiongray.d3m.csvinsights.core.service.stats.Summarizable;
import com.hyperiongray.d3m.csvinsights.core.service.stats.Summary;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.toList;

public abstract class StatsCalculator<T> implements Summarizable {

    private final int sizeWithoutNulls;
    protected List<? extends T> list;

    public StatsCalculator(List<? extends T> list) {
        this.list = list;
        this.sizeWithoutNulls = (int) list.stream().filter(Objects::nonNull).count();
    }

    @Override
    public Summary getSummary() {

        return Summary.newBuilder()
                .withCount(getCount())
                .withNulls(getNullsCount())
                .withNotNulls(getNotNullCount())
                .withDistinct(getDistinct())
                .withDistribution(getDistribution())
                .withIsNullable(isNullable())
                .withIsPrimaryKeyCandidate(isPkCandidate())
                .withIsUnique(isUnique())
                .build();

    }

    public int getCount() {
        return sizeWithoutNulls;
    }

    public int getNullsCount() {
        return sizeWithoutNulls - list.size();
    }

    public int getNotNullCount() {
        return list.size();
    }

    public int getDistinct() {
        return Long.valueOf(list.stream().distinct().count()).intValue();
    }

    public Optional<? extends T> getFirst() {
        return list.stream().findFirst();
    }

    public List<? extends T> getFrequentItems(long size) {
        return list.stream().collect(Collectors.groupingBy(x -> x, counting()))
                .entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .limit(size)
                .map(x -> x.getKey())
                .collect(toList());
    }

    public Map<String, Integer> getCountStats() {
        Map<String, Integer> stats = Maps.newLinkedHashMap();
        stats.put("count", getCount());
        stats.put("nulls", getNullsCount());
        stats.put("notNulls", getNotNullCount());
        stats.put("distinct", getDistinct());
        return stats;
    }

    public Optional<Stats> getMathStats() {
        return Optional.empty(); // returns empty by default
    }

    public Map<Integer, Double> getDistribution() {
        return Maps.newHashMap(); // returns empty by default
    }

    public Boolean isNullable() {
        return sizeWithoutNulls - getNotNullCount() > 0;
    }

    public Boolean isUnique() {
        return getCount() == getDistinct();
    }

    public Boolean isPkCandidate() {
        return !isNullable() && isUnique();
    }

    @Override
    public String toString() {
        return "SummaryCalculator{" +
                "size=" + sizeWithoutNulls +
                ", list=" + list +
                '}';
    }
}
