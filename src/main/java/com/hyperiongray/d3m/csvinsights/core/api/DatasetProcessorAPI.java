package com.hyperiongray.d3m.csvinsights.core.api;

import com.hyperiongray.d3m.csvinsights.core.model.Dataset;
import com.hyperiongray.d3m.csvinsights.core.source.reader.DatasetReaderConfig;

import java.net.URL;
import java.nio.file.Path;

public interface DatasetProcessorAPI {

    Dataset process(URL datasetUrl);

    Dataset process(URL datasetUrl, DatasetReaderConfig datasetReaderConfig);

    Dataset process(Path path);

    Dataset process(Path path, DatasetReaderConfig datasetReaderConfig);


}
