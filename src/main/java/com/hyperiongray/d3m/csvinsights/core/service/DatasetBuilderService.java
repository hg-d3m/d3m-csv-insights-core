package com.hyperiongray.d3m.csvinsights.core.service;

import com.google.common.collect.Lists;
import com.hyperiongray.d3m.csvinsights.core.model.Dataset;
import com.hyperiongray.d3m.csvinsights.core.model.Field;
import com.hyperiongray.d3m.csvinsights.core.model.Schema;
import com.hyperiongray.d3m.csvinsights.core.model.column.Column;
import com.hyperiongray.d3m.csvinsights.core.model.column.impl.*;
import com.hyperiongray.d3m.csvinsights.core.service.ner.NerClassifier;
import com.hyperiongray.d3m.csvinsights.core.source.RawDataset;

import java.util.List;


public class DatasetBuilderService {

    private final NerClassifier nerClassifier;

    public DatasetBuilderService(NerClassifier nerClassifier) {
        this.nerClassifier = nerClassifier;
    }

    public Dataset build(RawDataset rawDataset, Schema schema) {
        List<List<String>> inputAsColumn = RowsToColumnsConverter.transformRowsToColumns(rawDataset);
        List<Column> columns = Lists.newArrayList();

        for (int index = 0; index < schema.getFields().size(); index++) {
            List<String> strings = inputAsColumn.get(index);
            if (schema.hasHeaders()) {
                strings.remove(0);
            }
            Column column = create(schema.getFields().get(index), strings);
            columns.add(column);
        }

        int rowsCount = rawDataset.getRows().size();
        if (schema.hasHeaders()) {
            rowsCount = rowsCount - 1;
        }
        return Dataset.newBuilder()
                .withRows(rowsCount)
                .withColumns(columns)
                .withHeaders(schema.hasHeaders())
                .build();
    }

    private Column create(Field field, List<String> data) {

        switch (field.getParser().getPrimitiveFieldType()) {
            case LONG:
                return new LongColumn(field, data);
            case DOUBLE:
                return new DoubleColumn(field, data);
            case DATETIME:
                return new DateTimeColumn(field, data);
            case DATE:
                return new DateColumn(field, data);
            case TIME:
                return new TimeColumn(field, data);
            case BOOLEAN:
                return new BooleanColumn(field, data);
            case STRING:
                return new StringColumn(field, data, nerClassifier);
            default:
                throw new UnsupportedOperationException();
        }
    }

}
