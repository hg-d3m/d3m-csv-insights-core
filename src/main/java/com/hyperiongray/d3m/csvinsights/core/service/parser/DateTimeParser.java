package com.hyperiongray.d3m.csvinsights.core.service.parser;

import com.hyperiongray.d3m.csvinsights.core.model.PrimitiveFieldType;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import java.util.Optional;

public class DateTimeParser implements Parser<ZonedDateTime> {
    private final PrimitiveFieldType datetime = PrimitiveFieldType.DATETIME;

    private final DateTimeFormatter pattern;

    public DateTimeParser(DateTimeFormatter pattern) {
        super();
        this.pattern = pattern;
    }

    @Override
    public Optional<ZonedDateTime> parse(String input) {
        try {
            ZonedDateTime utc = LocalDateTime.parse(input, pattern).atZone(ZoneId.of("UTC"));
            return Optional.of(utc);
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    @Override
    public PrimitiveFieldType getPrimitiveFieldType() {
        return datetime;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DateTimeParser that = (DateTimeParser) o;
        return datetime == that.datetime &&
                Objects.equals(pattern, that.pattern);
    }

    @Override
    public int hashCode() {
        return Objects.hash(datetime, pattern);
    }

    @Override
    public String toString() {
        return "DateTimeParser{" +
                "datetime=" + datetime +
                ", pattern=" + pattern +
                '}';
    }
}
