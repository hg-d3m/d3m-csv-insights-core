package com.hyperiongray.d3m.csvinsights.core.service.ner;

public enum EnrType {
    PERSON, LOCATION, ORGANIZATION;
}
