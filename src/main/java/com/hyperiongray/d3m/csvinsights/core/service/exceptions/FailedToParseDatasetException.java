package com.hyperiongray.d3m.csvinsights.core.service.exceptions;

public class FailedToParseDatasetException extends RuntimeException {

    public FailedToParseDatasetException(String message) {
        super(message);
    }
}
