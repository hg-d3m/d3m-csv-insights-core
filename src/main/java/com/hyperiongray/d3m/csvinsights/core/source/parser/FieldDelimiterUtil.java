package com.hyperiongray.d3m.csvinsights.core.source.parser;

import com.google.common.collect.Sets;
import com.hyperiongray.d3m.csvinsights.core.service.util.CollectionUtils;

import java.util.*;

public class FieldDelimiterUtil {

    private static Set<Character> validSeparators = Sets.newHashSet(',', ';', '|', '\t');

    public static Optional<Character> inferDelimiterFromLine(String line) {
        Map<Character, Integer> freq = new HashMap<>();
        char[] chars = line.toCharArray();
        boolean quoteOpen = false;
        for (Character c : chars) {
            if (c == '\"') {
                quoteOpen = !quoteOpen;
                continue;
            }
            if (!quoteOpen) {
                if (!Character.isLetterOrDigit(c) && c != ' ') {
                    if (validSeparators.contains(c)) {
                        freq.merge(c, 1, (o, n) -> o + 1);
                    }
                }
            }
        }
        List<Character> valuesSorted = CollectionUtils.getKeysSortedDesc(freq);
        return valuesSorted.isEmpty() ? Optional.empty() : Optional.of(valuesSorted.get(0));
    }

}
