package com.hyperiongray.d3m.csvinsights.core.model.column.impl;

import com.google.common.collect.Lists;
import com.hyperiongray.d3m.csvinsights.core.model.Field;
import com.hyperiongray.d3m.csvinsights.core.service.parser.LongParser;
import com.hyperiongray.d3m.csvinsights.core.service.stats.Summarizable;
import com.hyperiongray.d3m.csvinsights.core.service.stats.Summary;
import com.hyperiongray.d3m.csvinsights.core.service.stats.summarycalculator.impl.NumericStatsCalculator;

import java.util.List;
import java.util.Optional;

import static com.hyperiongray.d3m.csvinsights.core.service.util.ClassUtils.convertInstanceOfObject;

public class LongColumn extends NumericColumn implements Summarizable {

    private final List<Long> data = Lists.newArrayList();
    private final LongParser parser;

    public LongColumn(Field field, List<String> inputs) {
        super(field);
        parser = convertInstanceOfObject(field.getParser(), LongParser.class);
        init(inputs);
    }

    protected void fill(List<String> inputs) {
        for (String input : inputs) {
            Optional<Long> aLong = parser.parse(input);
            data.add(aLong.orElse(null));
        }
    }

    public List<Long> getData() {
        return data;
    }

    @Override
    protected Summary calculateSummary() {
        return new NumericStatsCalculator(data).getSummary();
    }

}
