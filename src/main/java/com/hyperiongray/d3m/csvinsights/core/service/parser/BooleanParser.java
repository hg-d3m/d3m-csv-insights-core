package com.hyperiongray.d3m.csvinsights.core.service.parser;

import com.hyperiongray.d3m.csvinsights.core.model.PrimitiveFieldType;

import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;

public class BooleanParser implements Parser<Boolean> {
    private final PrimitiveFieldType primitiveFieldType = PrimitiveFieldType.BOOLEAN;

    private final Predicate<String> predicate;

    public BooleanParser(Predicate<String> predicate) {
        this.predicate = predicate;
    }

    @Override
    public Optional<Boolean> parse(String input) {
        return Optional.of(predicate.test(input));
    }

    @Override
    public PrimitiveFieldType getPrimitiveFieldType() {
        return primitiveFieldType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BooleanParser that = (BooleanParser) o;
        return primitiveFieldType == that.primitiveFieldType &&
                Objects.equals(predicate, that.predicate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(primitiveFieldType, predicate);
    }

    @Override
    public String toString() {
        return "BooleanParser{" +
                "primitiveFieldType=" + primitiveFieldType +
                ", predicate=" + predicate +
                '}';
    }
}
