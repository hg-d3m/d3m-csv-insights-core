package com.hyperiongray.d3m.csvinsights.core.service.stats.summarycalculator.impl;
import com.hyperiongray.d3m.csvinsights.core.service.stats.summarycalculator.StatsCalculator;


import java.util.List;

public class GenericStatsCalculator<T> extends StatsCalculator<T> {

    public GenericStatsCalculator(List<? extends T> list) {
        super(list);
    }
}
