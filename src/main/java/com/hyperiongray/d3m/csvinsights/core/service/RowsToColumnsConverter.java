package com.hyperiongray.d3m.csvinsights.core.service;

import com.hyperiongray.d3m.csvinsights.core.source.RawDataset;

import java.util.ArrayList;
import java.util.List;

public class RowsToColumnsConverter {

    public static List<List<String>> transformRowsToColumns(RawDataset rawDataset) {
        List<List<String>> rows = rawDataset.getRows();

        //Integer maxRowWidth = rows.stream().map(List::size).reduce(0, (a, b) -> a > b ? a : b); //aka number of columns

        int fileWidth = rawDataset.getFileWidth();
        List<List<String>> columns = new ArrayList<>(fileWidth);
        for (int i = 0; i < fileWidth; i++) {
            columns.add(new ArrayList<>(rows.size()));
        }

        for (List<String> row : rows) {
            for (int j = 0; j < fileWidth; j++) {
                if (j < row.size()) {
                    columns.get(j).add(row.get(j));
                } else {
                    columns.add(null); // pad with nulls at the end if the row gets narrower
                }
            }
        }

        return columns;
    }

}
