package com.hyperiongray.d3m.csvinsights.core.service.parser.factory;

import java.util.Locale;
import java.util.Optional;

public abstract class NumericParserFactory {

    protected Optional<Locale> inferLocale(String number) {

        if (number.indexOf('.') == -1) {
            if (number.indexOf(',') == -1) {
                return Optional.empty();
            } else if (number.lastIndexOf(',') == number.length() - 2) {
                return Optional.of(Locale.GERMANY);
            } else if (number.lastIndexOf(',') == number.length() - 3) {
                return Optional.of(Locale.US);
            }
        } else if (number.lastIndexOf('.') == number.length() - 2) {
            return Optional.of(Locale.US);
        } else if (number.lastIndexOf('.') == number.length() - 3) {
            return Optional.of(Locale.GERMANY);
        }
        return Optional.empty();
    }

}
