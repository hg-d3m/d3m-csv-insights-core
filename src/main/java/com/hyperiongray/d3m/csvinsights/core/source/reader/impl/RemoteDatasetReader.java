package com.hyperiongray.d3m.csvinsights.core.source.reader.impl;

import com.hyperiongray.d3m.csvinsights.core.source.reader.DatasetReader;
import com.hyperiongray.d3m.csvinsights.core.source.reader.DatasetReaderConfig;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;


public class RemoteDatasetReader extends DatasetReader {

    private final URL url;

    public RemoteDatasetReader(URL url, DatasetReaderConfig datasetReaderConfig) {
        super(datasetReaderConfig);
        this.url = url;
    }

    @Override
    protected InputStream getInputStream() throws IOException {
        validateContent(url);
        return url.openStream();
    }


    public void validateContent(URL url) {
        URLConnection conn = null;
        try {
            conn = url.openConnection();
            if (conn instanceof HttpURLConnection) {
                ((HttpURLConnection) conn).setRequestMethod("HEAD");
            }


            /*
            String headerFieldsContentType = conn.getHeaderField("Content-Type");
            if ("application/octet-stream".equals(headerFieldsContentType)) {
                throw new IllegalArgumentException("Invalid contentType:" + headerFieldsContentType);
            }
            */


            String headerFieldContentLength = conn.getHeaderField("Content-Length");
            if (headerFieldContentLength != null && Long.parseLong(headerFieldContentLength) > getDatasetReaderConfig().getMaxUrlContentSize()) {
                throw new IllegalArgumentException("File is too large");
            }

            if (headerFieldContentLength != null && Long.parseLong(headerFieldContentLength) == 0L) {
                throw new IllegalArgumentException("File is empty");
            }


        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            if (conn instanceof HttpURLConnection) {
                ((HttpURLConnection) conn).disconnect();
            }
        }
    }

    /*
    @Override
    public String peak() {
        String line = Strings.EMPTY;
        try (InputStream stream = url.openStream()) {
            LineIterator it = IOUtils.lineIterator(stream, StandardCharsets.UTF_8);
            if (it.hasNext()) {
                line = it.nextLine();
                line = trimBOM(line);
            }
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
        return line;
    }

    @Override
    public List<List<String>> readAll(String fieldsDelimiter, int limit) {
        List<List<String>> lines = Lists.newArrayList();
        boolean isFirstLine = true;
        try (InputStream stream = url.openStream()) {
            LineIterator it = IOUtils.lineIterator(stream, StandardCharsets.UTF_8);
            while (it.hasNext() && lines.size() < limit) {
                String line = it.nextLine();
                if (isFirstLine) {
                    line = trimBOM(line);
                    isFirstLine = false;
                }
                List<String> fields = FieldParserUtil.parseLine(line, fieldsDelimiter.charAt(0));
                lines.add(fields);
            }

        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
        return lines;
    }
*/


}
