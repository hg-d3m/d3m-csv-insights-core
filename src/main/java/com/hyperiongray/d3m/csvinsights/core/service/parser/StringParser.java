package com.hyperiongray.d3m.csvinsights.core.service.parser;

import com.hyperiongray.d3m.csvinsights.core.model.PrimitiveFieldType;

import java.util.Objects;
import java.util.Optional;

public class StringParser implements Parser<String> {
    private final PrimitiveFieldType primitiveFieldType = PrimitiveFieldType.STRING;

    @Override
    public Optional<String> parse(String input) {
        return Optional.of(input);
    }

    @Override
    public PrimitiveFieldType getPrimitiveFieldType() {
        return primitiveFieldType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StringParser that = (StringParser) o;
        return primitiveFieldType == that.primitiveFieldType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(primitiveFieldType);
    }

    @Override
    public String toString() {
        return "StringParser{" +
                "string=" + primitiveFieldType +
                '}';
    }
}
