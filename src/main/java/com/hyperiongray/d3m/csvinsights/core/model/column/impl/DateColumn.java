package com.hyperiongray.d3m.csvinsights.core.model.column.impl;

import com.google.common.collect.Lists;
import com.hyperiongray.d3m.csvinsights.core.model.Field;
import com.hyperiongray.d3m.csvinsights.core.service.parser.DateParser;
import com.hyperiongray.d3m.csvinsights.core.model.column.Column;
import com.hyperiongray.d3m.csvinsights.core.service.stats.Summarizable;
import com.hyperiongray.d3m.csvinsights.core.service.stats.Summary;
import com.hyperiongray.d3m.csvinsights.core.service.stats.summarycalculator.impl.GenericStatsCalculator;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static com.hyperiongray.d3m.csvinsights.core.service.util.ClassUtils.convertInstanceOfObject;

public class DateColumn extends Column implements Summarizable {

    private final List<LocalDate> data = Lists.newArrayList();
    private DateParser parser;

    public DateColumn(Field field, List<String> inputs) {
        super(field);
        parser = convertInstanceOfObject(field.getParser(), DateParser.class);
        init(inputs);
    }

    @Override
    protected void fill(List<String> inputs) {
        for (String input : inputs) {
            Optional<LocalDate> optional = parser.parse(input);
            data.add(optional.orElse(null));
        }
    }

    public List<LocalDate> getData() {
        return data;
    }

    @Override
    protected Summary calculateSummary() {
        return new GenericStatsCalculator<>(data).getSummary();
    }

}
