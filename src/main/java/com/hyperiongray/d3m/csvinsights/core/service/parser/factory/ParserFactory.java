package com.hyperiongray.d3m.csvinsights.core.service.parser.factory;

import com.hyperiongray.d3m.csvinsights.core.service.parser.Parser;

import java.util.List;

public interface ParserFactory<T> {
    List<Parser<T>> findParser(String input);
}
