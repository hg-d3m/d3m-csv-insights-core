# D3M CSV Insights!

Multi-purpose CSV to Java parser.

## Example:
A CSV with this data

```
Country, Ranking, Country2, GDP
USA,1,United States,"19,390,604"
CHN,2,China,"12,237,700"
JPN,3,Japan,"4,872,137"
DEU,4,Germany,"3,677,439"
...
```
would be parsed to 

```javascript
{
    "rows": 201,
    "columns":
    [{
        "field": {"columnIndex": 0, "name": "Country", "parser": {"primitiveFieldType": "STRING"}},
        "summary": {
            "count": 201,
            "nulls": 0,
            "notNulls": 201,
            "distinct": 201,
            "distribution": {},
            "nullable": false,
            "primaryKeyCandidate": true,
            "unique": true
        },
        "sample": {
            "sample": ["USA", "CHN", "JPN", "DEU", "GBR", "IND", "FRA", "BRA", "ITA", "CAN"], "frequent": {
                "NZL": 1,
                "FJI": 1,
                "PNG": 1,
                "STP": 1,
                "MHL": 1,
                ...
            }
        },
        "enrType": "ORGANIZATION",
        "data": ["USA", "CHN", "JPN", "DEU", "GBR", "IND", "FRA", "BRA", "ITA", "CAN", "RUS", "KOR", "AUS", "ESP", "MEX", "IDN", "TUR", "NLD", "SAU", "CHE", "ARG", "SWE", "POL", "BEL", "THA", "IRN", "AUT", "NOR", "ARE", "NGA", "ISR", "ZAF", "HKG", "IRL", "DNK", "SGP", "MYS", "COL", "PHL", "PAK", "CHL", "FIN", "BGD", "EGY", "VNM", "PRT", "CZE", "ROU", "PER", "NZL", "GRC", "IRQ", "DZA", "QAT", "KAZ", "HUN", "AGO", "KWT", "SDN", "UKR", "MAR", "ECU", "PRI", "CUB", "SVK", "LKA", "ETH", "KEN", "DOM", "GTM", "OMN", "MMR", "LUX", "PAN", "GHA", "BGR", "CRI", "URY", "HRV", "BLR", "LBN", "TZA", "MAC", "UZB", "SVN", "LTU", "SRB", "AZE", "JOR", "TUN", "PRY", "LBY", "TKM", "COD", "BOL", "CIV", "BHR", "CMR", "YEM", "LVA", "UGA", "EST", "ZMB", "NPL", "SLV", "ISL", "HND", "KHM", "TTO", "CYP", "ZWE", "SEN", "PNG", "AFG", "BIH", "BWA", "LAO", "MLI", "GEO", "GAB", "JAM", "PSE", "NIC", "MUS", "NAM", "ALB", "MOZ", "MLT", "BFA", "GNQ", "BHS", "BRN", "ARM", "MDG", "MNG", "MKD", "GIN", "TCD", "BEN", "RWA", "COG", "HTI", "MDA", "NER", "KGZ", "XKX", "TJK", "SOM", "IMN", "MWI", "LIE", "GUM", "FJI", "MRT", "MDV", "MNE", "TGO", "BRB", "SWZ", "SLE", "VIR", "GUY", "LBR", "BDI", "AND", "SUR", "TLS", "SSD", "GRL", "ABW", "LSO", "BTN", "FRO", "CAF", "BLZ", "DJI", "CPV", "LCA", "SMR", "MNP", "ATG", "SYC", "GMB", "GNB", "SLB", "GRD", "COM", "KNA", "VUT", "WSM", "VCT", "ASM", "DMA", "TON", "STP", "FSM", "PLW", "MHL", "KIR", "NRU", "TUV"]
    }, {
        "field": {"columnIndex": 1, "name": "Ranking", "parser": {"primitiveFieldType": "LONG"}},
        "summary": {
            "count": 201,
            "nulls": 0,
            "notNulls": 201,
            "distinct": 201,
            "distribution": {"25": 51.0, "50": 101.0, "75": 151.0, "90": 181.0, "99": 199.0},
            "nullable": false,
            "primaryKeyCandidate": true,
            "unique": true
        },
        "sample": {
            "sample": ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"], "frequent": {
                "110": 1,
                "111": 1,
                "112": 1,
                "113": 1,
                ...
            }
        },
        "data": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200, 201]
    }, {
        "field": {"columnIndex": 2, "name": "Country2", "parser": {"primitiveFieldType": "STRING"}},
        "summary": {
            "count": 201,
            "nulls": 0,
            "notNulls": 201,
            "distinct": 201,
            "distribution": {},
            "nullable": false,
            "primaryKeyCandidate": true,
            "unique": true
        },
        "sample": {
            "sample": ["United States", "China", "Japan", "Germany", "United Kingdom", "India", "France", "Brazil", "Italy", "Canada"],
            "frequent": {
                "Papua New Guinea": 1,
                "Cambodia": 1,
                "Paraguay": 1,
                "Kazakhstan": 1,
                "Solomon Islands": 1,
                "Macedonia, FYR": 1,
                "Marshall Islands": 1,
                "Mali": 1,
                "Panama": 1,
                "Hong Kong SAR, China": 1,
                "Argentina": 1,
               ....
            }
        },
        "enrType": "LOCATION",
        "data": ["United States", "China", "Japan", "Germany", "United Kingdom", "India", "France", "Brazil", "Italy", "Canada", "Russian Federation", "Korea, Rep.", "Australia", "Spain", "Mexico", "Indonesia", "Turkey", "Netherlands", "Saudi Arabia", "Switzerland", "Argentina", "Sweden", "Poland", "Belgium", "Thailand", "Iran, Islamic Rep.", "Austria", "Norway", "United Arab Emirates", "Nigeria", "Israel", "South Africa", "Hong Kong SAR, China", "Ireland", "Denmark", "Singapore", "Malaysia", "Colombia", "Philippines", "Pakistan", "Chile", "Finland", "Bangladesh", "Egypt, Arab Rep.", "Vietnam", "Portugal", "Czech Republic", "Romania", "Peru", "New Zealand", "Greece", "Iraq", "Algeria", "Qatar", "Kazakhstan", "Hungary", "Angola", "Kuwait", "Sudan", "Ukraine", "Morocco", "Ecuador", "Puerto Rico", "Cuba", "Slovak Republic", "Sri Lanka", "Ethiopia", "Kenya", "Dominican Republic", "Guatemala", "Oman", "Myanmar", "Luxembourg", "Panama", "Ghana", "Bulgaria", "Costa Rica", "Uruguay", "Croatia", "Belarus", "Lebanon", "Tanzania", "Macao SAR, China", "Uzbekistan", "Slovenia", "Lithuania", "Serbia", "Azerbaijan", "Jordan", "Tunisia", "Paraguay", "Libya", "Turkmenistan", "Congo, Dem. Rep.", "Bolivia", "Côte d'Ivoire", "Bahrain", "Cameroon", "Yemen, Rep.", "Latvia", "Uganda", "Estonia", "Zambia", "Nepal", "El Salvador", "Iceland", "Honduras", "Cambodia", "Trinidad and Tobago", "Cyprus", "Zimbabwe", "Senegal", "Papua New Guinea", "Afghanistan", "Bosnia and Herzegovina", "Botswana", "Lao PDR", "Mali", "Georgia", "Gabon", "Jamaica", "West Bank and Gaza", "Nicaragua", "Mauritius", "Namibia", "Albania", "Mozambique", "Malta", "Burkina Faso", "Equatorial Guinea", "Bahamas, The", "Brunei Darussalam", "Armenia", "Madagascar", "Mongolia", "Macedonia, FYR", "Guinea", "Chad", "Benin", "Rwanda", "Congo, Rep.", "Haiti", "Moldova", "Niger", "Kyrgyz Republic", "Kosovo", "Tajikistan", "Somalia", "Isle of Man", "Malawi", "Liechtenstein", "Guam", "Fiji", "Mauritania", "Maldives", "Montenegro", "Togo", "Barbados", "Eswatini", "Sierra Leone", "Virgin Islands (U.S.)", "Guyana", "Liberia", "Burundi", "Andorra", "Suriname", "Timor-Leste", "South Sudan", "Greenland", "Aruba", "Lesotho", "Bhutan", "Faroe Islands", "Central African Republic", "Belize", "Djibouti", "Cabo Verde", "St. Lucia", "San Marino", "Northern Mariana Islands", "Antigua and Barbuda", "Seychelles", "Gambia, The", "Guinea-Bissau", "Solomon Islands", "Grenada", "Comoros", "St. Kitts and Nevis", "Vanuatu", "Samoa", "St. Vincent and the Grenadines", "American Samoa", "Dominica", "Tonga", "São Tomé and Principe", "Micronesia, Fed. Sts.", "Palau", "Marshall Islands", "Kiribati", "Nauru", "Tuvalu"]
    }, {
        "field": {"columnIndex": 3, "name": "GDP", "parser": {"primitiveFieldType": "LONG"}},
        "summary": {
            "count": 201,
            "nulls": 0,
            "notNulls": 201,
            "distinct": 201,
            "distribution": {"25": 6215.0, "50": 25995.0, "75": 200288.0, "90": 637430.0, "99": 4872137.0},
            "nullable": false,
            "primaryKeyCandidate": true,
            "unique": true
        },
        "sample": {
            "sample": ["19,390,604", "12,237,700", "4,872,137", "3,677,439", "2,622,434", "2,600,818", "2,582,501", "2,055,506", "1,934,798", "1,653,043"],
            "frequent": {
                "122,124": 1,
                "38,108": 1,
                "21,070": 1,
                "1,577,524": 1,
                "2,055,506": 1,
                "1,530,751": 1,
                "5,061": 1,
                ...
            }
        },
        "data": [19390604, 12237700, 4872137, 3677439, 2622434, 2600818, 2582501, 2055506, 1934798, 1653043, 1577524, 1530751, 1323421, 1311320, 1150888, 1015539, 851549, 826200, 686738, 678887, 637430, 538040, 526466, 492681, 455303, 454013, 416596, 398832, 382575, 375745, 350851, 348872, 341449, 333731, 324872, 323907, 314710, 314458, 313595, 304952, 277076, 251885, 249724, 235369, 223780, 217571, 215726, 211884, 211389, 205853, 200288, 192061, 167555, 166929, 162887, 139135, 122124, 120126, 117488, 112154, 109709, 104296, 103961, 96851, 95769, 87357, 80561, 79263, 75932, 75620, 72643, 67069, 62404, 62284, 58997, 58221, 57286, 56157, 55213, 54456, 53577, 52090, 50361, 49677, 48770, 47168, 41432, 40748, 40068, 39952, 39667, 38108, 37926, 37642, 37509, 37353, 35307, 34923, 31268, 30264, 25995, 25921, 25868, 24880, 24805, 23909, 22979, 22158, 22079, 22054, 22041, 21070, 20536, 19544, 18055, 17407, 16853, 15334, 15081, 15014, 14781, 14498, 13814, 13266, 13254, 13039, 12646, 12518, 12323, 12294, 12162, 12128, 11537, 11500, 11434, 11280, 10473, 9871, 9247, 9135, 8701, 8408, 8128, 8120, 7565, 7245, 7146, 7052, 6593, 6303, 6215, 5859, 5061, 5025, 4866, 4845, 4758, 4674, 4434, 3775, 3759, 3621, 3285, 3172, 3013, 2996, 2955, 2904, 2706, 2701, 2578, 2528, 2477, 1949, 1863, 1845, 1773, 1738, 1633, 1593, 1510, 1498, 1489, 1347, 1303, 1127, 1068, 992, 863, 841, 785, 634, 497, 428, 393, 336, 290, 204, 186, 114, 40]
    }]
}
```

## Other features

- Date recognizable formats:  
```
    "yyyy-MM-dd",
    "yyMMdd",
    "yyyyMMdd",
    "dd/MM/yy",
    "dd/MM/yyyy",
    "MM/dd/yy",
    "MM/dd/yyyy",
    "dd-MM-yyyy",
    "MM-dd-yyyy"
```

- Longs and doubles can also be parsed from these formats:
```
    "1,234.56"
    "1.234,56"
```    


### Pending:

#### More subtypes:
- Percentages
