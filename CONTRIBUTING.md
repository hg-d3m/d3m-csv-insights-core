# To perform a release

- checkout from develop to a new branch `rc`
- run mvn release:prepare (accepting the defaults)
- git push origin rc --tags 
- merge this branch to develop
- merge develop to master